package br.com.sanfit.pageobjects;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class MotivosAlteracoesNotasFiscaisPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/tabelas/motivosAlteracoesNotasFiscais/motivosAlteracoesNotasFiscais.jsp";

	public MotivosAlteracoesNotasFiscaisPage() {
		super(path);

	}

	public String registro;
	public boolean campoLimpo;
	public boolean exibirInfoCadastrais;
	public String motivo = "Testes Para Verificar";
	String msgConfirmacaoIncluir = "Registro incluído com sucesso!";
	String msgConfirmacaoExcluir = "Registro excluído com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Registro atualizado com sucesso!";
	By btnNovoMotivo = By.id("botaoNovoMotivosAlteracoesNotasFiscais");
	By btnIncluirMotivo = By.id("botaoIncluirMotivosAlteracoesNotasFiscais");
	By campoMotivo = By.id("motivo");
	By campoBuscaMotivo = By.id("motivoBusca");
	By btnLocalizarMotivo = By
			.id("botaoLocalizarMotivosAlteracoesNotasFiscais");
	By btnPesquisar = By.id("botaoPesquisar");
	By btnExcluir = By.id("botaoExcluirMotivosAlteracoesNotasFiscais");
	By btnAtualizarMotivo = By
			.id("botaoAtualizarMotivosAlteracoesNotasFiscais");
	By btnLimparCampoMotivo = By.id("botaoLimparMotivosAlteracoesNotasFiscais");
	By btnInfCadastrais = By.id("botaoCadastraisMotivosAlteracoesNotasFiscais");
	By registrosEncontrados = By.cssSelector("#divExibirRegistrosMotivos");
	By infoCadastrais = By
			.cssSelector("#divInformacoesCadastraisMotivosAlteracoesNotasFiscais");
	By btnFecharInformacoesCadastrais = By.xpath(".//*[@id='divInformacoesCadastraisMotivosAlteracoesNotasFiscais']/div/fieldset/div[1]/a/img");
	By btnFecharLocalizarMotivos = By
			.cssSelector("#fecharDivLocalizar>img");
//	By btnFecharInformacoesCadastrais = By
//			.cssSelector("#divInformacoesCadastraisMotivosAlteracoesNotasFiscais");
	By gridMotivos = By
			.cssSelector(".gridCelula.ng-binding");
	
	By btnRelatorio = By.id("botaoRelatorioMotivosAlteracoesNotasFiscais");
	By btnVisualizarRelatorio = By.cssSelector("#botaoVisualizarRelatorioMotivosAlteracoesNotasFiscais");
	By btnFecharRelatorio = By.cssSelector("#divVisualizarRelatorioMotivosAlteracoesNotasFiscais");			
	By fecharPDF = By.cssSelector("#divVisualizarRelatorioMotivosAlteracoesNotasFiscais");
	By Testejp = By.className("#divVisualizarRelatorioMotivosAlteracoesNotasFiscais > div > form > fieldset > div.tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble > a > img"); 
	By btnDesativarMotivos = By.id("botaoDesativarFiscais");
	By btnAtivarMotivos = By.id("botaoAtivarFiscais");
	
	WebDriverWait wait = new WebDriverWait(driver, 30);

	public MotivosAlteracoesNotasFiscaisPage clickBtnNovoMotivo() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnNovoMotivo));
		driver.findElement(btnNovoMotivo).click();

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage preencherCampoMotivo() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoMotivo));
		driver.findElement(campoMotivo).clear();
		driver.findElement(campoMotivo).sendKeys(motivo);

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clickBtnIncluirMotivo() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnIncluirMotivo));
		driver.findElement(btnIncluirMotivo).click();

		return this;
	}
	
	
	
	public MotivosAlteracoesNotasFiscaisPage clickGridMotivo() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(gridMotivos));
		driver.findElement(gridMotivos).click();
		return this;
	}
	
	public MotivosAlteracoesNotasFiscaisPage clickDesativar() throws InterruptedException{
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnDesativarMotivos));
		Thread.sleep(2000);
		driver.findElement(btnDesativarMotivos).click();
		
		return this;
	}
	
	public MotivosAlteracoesNotasFiscaisPage clickAtivar() throws InterruptedException{
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtivarMotivos));
		Thread.sleep(2000);
		driver.findElement(btnAtivarMotivos).click();
		
		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage verificarAlertIncluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage verificarAlertConfirmarExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmarExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage verificarAlertExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage verificarAlertAtualizar() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAtualizacao);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clickBtnLocalizarMotivo()
			throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnLocalizarMotivo));
		Thread.sleep(2000);
		driver.findElement(btnLocalizarMotivo).click();

		return this;
	}
	
	public MotivosAlteracoesNotasFiscaisPage clickBtnRelatorio()
			throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnRelatorio).click();

		return this;
	}
	
	public MotivosAlteracoesNotasFiscaisPage clickBtnVisualizarRelatorio()
			throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnVisualizarRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnVisualizarRelatorio).click();

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clickBtnPesquisarMotivo() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clickBtnExcluir()
			throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnExcluir));
		Thread.sleep(2000);
		driver.findElement(btnExcluir).click();

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clickBtnInfCadastrais()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnInfCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnInfCadastrais).click();

		return this;
	}
	
	public MotivosAlteracoesNotasFiscaisPage clickBtnFecharInfCadastrais()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnFecharInformacoesCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnFecharInformacoesCadastrais).click();

		return this;
	}
	

	public MotivosAlteracoesNotasFiscaisPage realizarBuscaMotivo() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(campoBuscaMotivo));
		driver.findElement(campoBuscaMotivo).clear();
		driver.findElement(campoBuscaMotivo).sendKeys(motivo);

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clicKFecharLocalizarMotivos() {

		driver.findElement(btnFecharLocalizarMotivos).click();

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clickBtnFecharInformacoesFiscais() {
		driver.findElement(btnFecharInformacoesCadastrais).click();
		return this;
	}
	
	public MotivosAlteracoesNotasFiscaisPage clickBtnFecharRelatorio() {
		driver.findElement(btnFecharRelatorio).isDisplayed();
				
		return this;
	}
	
	public MotivosAlteracoesNotasFiscaisPage listaDeRegistrosEncontrados()
			throws InterruptedException {
		motivo = motivo.toUpperCase();
		Thread.sleep(2000);
		registro = driver.findElement(registrosEncontrados).getText()
				.toUpperCase();

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage clickBtnAtualizarMotivo()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnAtualizarMotivo));
		Thread.sleep(2000);
		driver.findElement(btnAtualizarMotivo).click();

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage limparMotivo()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnLimparCampoMotivo));
		Thread.sleep(2000);
		driver.findElement(btnLimparCampoMotivo).click();
		campoLimpo = driver.findElement(campoBuscaMotivo).getText().isEmpty();

		return this;
	}

	public MotivosAlteracoesNotasFiscaisPage informacoesCadastraisMotivo()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(infoCadastrais));
		Thread.sleep(2000);
		exibirInfoCadastrais = driver.findElement(infoCadastrais).isDisplayed();

		return this;
	}

}
