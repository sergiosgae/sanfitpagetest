package br.com.sanfit.pageobjects;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PlantoesFiscaisPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/cadastrar/plantoesFiscais/plantoesFiscais.jsp";
	public PlantoesFiscaisPage() {
		super(path);

	}

	public String registro;
	public boolean campoLimpo;
	public boolean exibirInfoCadastrais;

	String msgConfirmacaoIncluir = "Registro incluído com sucesso!";
	String msgConfirmacaoExcluir = "Registro excluído com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Registro atualizado com sucesso!";
	By btnLupaFiscal = By.id("botaoLocalizarLotacoes");
	
	By btnLocalizarLotacoes = By.id("botaoLupaLocalizarUsuarios");
	By campoFiscal = By.cssSelector("#nomeBusca");
	public String fiscal = "WIRON VILAROUCA FELIX DE FREITAS";
	By btnPesquisar = By.id("botaoPesquisar");
	By marcarFiscal = By.id("botaoPesquisar");
	By gridPlantoesFiscais = By.cssSelector(".gridCelula.ng-binding");

	By btnLocalizarCidades = By.id("botaoLocalizarCidades");

	By btnNovoPostoFiscal = By.id("botaoNovoPlantoesFiscais");
	By nomeBusca = By.className("ng-pristine ng-valid ng-empty ng-valid-maxlength ng-touched");
	public String cidade = "ABADIA DE GOIÁS";
	public String cexat = "NUCLEO DE ATENDIMENTO E MONITORAMENTO EM CAUCAIA";

	By btnIncluirPlantoesFiscais = By.id("botaoIncluirPlantoesFiscais");
	By comboPlantoesFiscais = By.id("codigoCexat");
	String valorCampoCexat = "LOTAÇÃO INICIAL";

	By campoBuscaPlantoesFiscais = By.id("nomeBusca");
	By campoLotacao = By.id("lotacao");

	By btnLocalizarPlantoesFiscais = By.id("botaoLocalizarPlantoesFiscais");

	By btnExcluir = By.id("botaoExcluirPlantoesFiscais");
	By btnAtualizarPlantoesFiscais = By.id("botaoAtualizarPlantoesFiscais");
	By btnLimparCampoPlantoesFiscais = By.id("botaoLimparPlantoesFiscais");
	By btnInfCadastrais = By.id("botaoCadastraisPlantoesFiscais");
	By registrosEncontrados = By.cssSelector("#divExibirRegistrosPlantoesFiscais");
	By infoCadastrais = By.cssSelector("#divInformacoesCadastraisPlantoesFiscaissAlteracoesNotasFiscais");
	By btnFecharInformacoesCadastrais = By.xpath(".//*[@id='divInformacoesCadastrais']/div/fieldset/div[1]/a/img");
	By btnFecharLocalizarPlantoesFiscaiss = By.cssSelector(".tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble>a>img");
	By btnFecharLotacoes = By.cssSelector("#btnFecharDivLocalizarArvoresAssuntos>img");
	By gridPlantoesFiscaiss = By.cssSelector(".gridCelula.ng-binding");

	By btnRelatorio = By.id("botaoRelatorioPlantoesFiscais");
	By btnVisualizarRelatorio = By.cssSelector("#botaoVisualizarRelatorioPlantoesFiscais");

	By divRelatorio = By.xpath(".//*[@id='divVisualizarRelatorioPlantoesFiscais']/div");
	By btnFecharRelatorio = By.xpath(".//*[@id='divVisualizarRelatorioPlantoesFiscais']/div/form/fieldset/div[1]/a/img");

	By fecharPDF = By.cssSelector("#divVisualizarRelatorioPlantoesFiscaissAlteracoesNotasFiscais");
	By Testejp = By.className(
			"#divVisualizarRelatorioPlantoesFiscaissAlteracoesNotasFiscais > div > form > fieldset > div.tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble > a > img");
	By btnDesativarPlantoesFiscaiss = By.id("botaoDesativarFiscais");
	By btnAtivarPlantoesFiscaiss = By.id("botaoAtivarFiscais");

	WebDriverWait wait = new WebDriverWait(driver, 30);
	Actions actions = new Actions(driver);

	public PlantoesFiscaisPage clickBtnNovoPlantoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnNovoPostoFiscal));
		Thread.sleep(1000);
		driver.findElement(btnNovoPostoFiscal).click();
		Thread.sleep(1000);
		return this;
	}

	public PlantoesFiscaisPage clickBtnLupaFiscal() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLupaFiscal));
		Thread.sleep(1000);
		driver.findElement(btnLupaFiscal).click();
		return this;
	}
	

	public PlantoesFiscaisPage localizarCexatsCidade() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLocalizarCidades));
		driver.findElement(btnLocalizarCidades).click();
		return this;
	}


	public PlantoesFiscaisPage digitarFiscal() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoFiscal));
		Thread.sleep(1000);
		driver.findElement(campoFiscal).sendKeys(fiscal);
		return this;
	}

	public PlantoesFiscaisPage clickDuplo() throws InterruptedException {
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath(".//*[@id='lotacao-1']/td"));
		Thread.sleep(1000);
		actions.doubleClick(element).perform();
		return this;
	}

	public PlantoesFiscaisPage SelecionaCexatsCombo() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboPlantoesFiscais));
		Thread.sleep(1000);
		WebElement element = driver.findElement(comboPlantoesFiscais);
		Thread.sleep(1000);
		Select combo = new Select(element);
		combo.selectByVisibleText(valorCampoCexat);
		Thread.sleep(1000);
		return this;
	}

	public PlantoesFiscaisPage clickBtnIncluirFiscal() throws InterruptedException {

		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		return this;
	}
	
	
	public PlantoesFiscaisPage checkMarcarFiscal() throws InterruptedException {

		wait.until(ExpectedConditions.visibilityOfElementLocated(marcarFiscal));
		driver.findElement(marcarFiscal).click();
		return this;
	}


	public PlantoesFiscaisPage localizarPlantoesFiscaisCidade() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLocalizarCidades));
		driver.findElement(btnLocalizarCidades).click();
		return this;
	}

	public PlantoesFiscaisPage clickBtnIncluirPlantoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirPlantoesFiscais));
		driver.findElement(btnIncluirPlantoesFiscais).click();
		return this;
	}

	public PlantoesFiscaisPage verificarAlertConfirmarInclusao() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluir);
			alert.accept();
		} catch (Exception e) {

		}
		return this;
	}

	public PlantoesFiscaisPage clickBtnLocalizarPlantoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(btnLocalizarPlantoesFiscais));
		Thread.sleep(1000);
		driver.findElement(btnLocalizarPlantoesFiscais).click();
		return this;
	}

	public PlantoesFiscaisPage realizarBuscaPlantoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoBuscaPlantoesFiscais));
		Thread.sleep(1000);
		driver.findElement(campoBuscaPlantoesFiscais).clear();
		Thread.sleep(1000);
		driver.findElement(campoBuscaPlantoesFiscais).sendKeys(cexat);
		return this;
	}

	public PlantoesFiscaisPage clickBtnPesquisarPlantoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(btnPesquisar).click();
		return this;
	}

	public PlantoesFiscaisPage clicKFecharLocalizarPlantoesFiscais() {

		driver.findElement(btnFecharLocalizarPlantoesFiscaiss).click();

		return this;
	}

	public PlantoesFiscaisPage clicKBtnAtualizarPlantoesFiscais() {
		driver.findElement(btnAtualizarPlantoesFiscais).click();
		return this;
	}

	public PlantoesFiscaisPage listaDeRegistrosEncontrados() throws InterruptedException {
		cexat = cexat.toUpperCase();
		Thread.sleep(2000);
		registro = driver.findElement(registrosEncontrados).getText().toUpperCase();

		return this;
	}

	public PlantoesFiscaisPage verificarAlertAtualizar() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAtualizacao);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PlantoesFiscaisPage clickGridPlantoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(gridPlantoesFiscaiss));
		Thread.sleep(1000);
		driver.findElement(gridPlantoesFiscaiss).click();
		return this;
	}

	public PlantoesFiscaisPage clickDesativar() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnDesativarPlantoesFiscaiss));
		Thread.sleep(1000);
		driver.findElement(btnDesativarPlantoesFiscaiss).click();

		return this;
	}

	public PlantoesFiscaisPage clickAtivar() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtivarPlantoesFiscaiss));
		Thread.sleep(1000);
		driver.findElement(btnAtivarPlantoesFiscaiss).click();

		return this;
	}

	public PlantoesFiscaisPage verificarAlertExcluir() {
		try {
			Thread.sleep(1000);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PlantoesFiscaisPage perguntaAlertExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmarExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PlantoesFiscaisPage clickBtnRelatorio() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnRelatorio).click();

		return this;
	}

	public PlantoesFiscaisPage clickBtnVisualizarRelatorio() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnVisualizarRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnVisualizarRelatorio).click();

		return this;
	}

	public PlantoesFiscaisPage clickBtnExcluir() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnExcluir));
		Thread.sleep(1000);

		driver.findElement(btnExcluir).click();

		return this;
	}

	public PlantoesFiscaisPage clickBtnInfCadastrais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnInfCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnInfCadastrais).click();

		return this;
	}

	public PlantoesFiscaisPage clickBtnFecharInfCadastrais() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharInformacoesCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnFecharInformacoesCadastrais).click();

		return this;
	}

	public PlantoesFiscaisPage clickBtnFecharInformacoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(btnFecharInformacoesCadastrais).click();
		return this;
	}

	public PlantoesFiscaisPage clickBtnFecharRelatorio() {

		driver.close();

		return this;
	}

	public PlantoesFiscaisPage clickBtnAtualizarPlantoesFiscais() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtualizarPlantoesFiscais));
		Thread.sleep(2000);
		driver.findElement(btnAtualizarPlantoesFiscais).click();
		return this;
	}

	public PlantoesFiscaisPage limparPlantoesFiscais() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLimparCampoPlantoesFiscais));
		Thread.sleep(1000);
		driver.findElement(btnLimparCampoPlantoesFiscais).click();
		Thread.sleep(1000);
		campoLimpo = driver.findElement(campoLotacao).getText().isEmpty();

		return this;
	}

	public PlantoesFiscaisPage informacoesCadastraisPlantoesFiscais() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(infoCadastrais));
		Thread.sleep(2000);
		exibirInfoCadastrais = driver.findElement(infoCadastrais).isDisplayed();

		return this;
	}

}
