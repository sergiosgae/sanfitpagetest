package br.com.sanfit.pageobjects;

import org.openqa.selenium.WebDriver;

import br.com.sanfit.setup.ConfigUtils;
import br.com.sanfit.setup.SeleniumDriverFactory;

public abstract class BasePage {

	
	public static WebDriver driver = SeleniumDriverFactory.getDriver();
	private String path;
	
	public BasePage(String path){
		this.path = path;
	}

    public void navegarParaOutraPagina(BasePage page) {
    	driver.navigate().to(ConfigUtils.getServerUrl() + page.path);
    }
    
    public void navegarPara() {
    	driver.navigate().to(ConfigUtils.getServerUrl() + path);
    }
}
