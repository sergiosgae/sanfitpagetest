package br.com.sanfit.pageobjects;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CexatsPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/cadastrar/cexats/cexats.jsp";

	public CexatsPage() {
		super(path);

	}

	public String registro;
	public boolean campoLimpo;
	public boolean exibirInfoCadastrais;

	String msgConfirmacaoIncluir = "Registro inclu�do com sucesso!";
	String msgConfirmacaoExcluir = "Registro inclu�do com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Registro atualizado com sucesso!";

	By btnNovoCexats = By.id("botaoNovoCexats");
	By btnLocalizarLotacoes = By.id("botaoLocalizarLotacoes");
	By lotacaoBusca = By.id("lotacaoBusca");
	public String lotacao = "ASSESSORIA JURIDICA";
	By btnPesquisar = By.id("botaoPesquisar");
	
	By gridCexats = By
			.cssSelector(".gridCelula.ng-binding");

	By btnLocalizarCidades = By.id("botaoLocalizarCidades");
	By nomeBusca = By.id("nomeBusca");
	public String cidade = "ABAETETUBA";
	public String cexat = "NUCLEO DE ATENDIMENTO E MONITORAMENTO EM CAUCAIA";

	By btnIncluirCexats = By.id("botaoIncluirCexats");
	By comboCexats = By.id("codigoCexat");
	By comboUf = By.id("codigoUF");
	By codigoMunicipio = By.id("codigoMunicipio");
	String valorCampoCexat = "CELULA DE EDUCACAO FISCAL E RESPONSABILIDADE SOCIOAMBIENTAL";
	String uf = "AC";

	By campoBuscaCexats = By.id("nomeBusca");
	By campoLotacao = By.id("lotacao");

	By btnLocalizarCexats = By.id("botaoLocalizarCexats");

	By btnExcluir = By.id("botaoExcluirCexats");
	By btnAtualizarCexats = By.id("botaoAtualizarCexats");
	By btnLimparCampoCexats = By.id("botaoLimparCexats");
	By btnInfCadastrais = By.id("botaoCadastraisCexats");
	By registrosEncontrados = By.cssSelector("#divExibirRegistrosCexats");
	By infoCadastrais = By.cssSelector("#divInformacoesCadastraisCexatssAlteracoesNotasFiscais");
	By btnFecharInformacoesCadastrais = By
			.xpath(".//*[@id='divInformacoesCadastrais']/div/fieldset/div[1]/a/img");
	By btnFecharLocalizarCexatss = By.cssSelector(".tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble>a>img");
	By btnFecharLotacoes = By
 .cssSelector("#btnFecharDivLocalizarArvoresAssuntos>img");
	By gridCexatss = By.cssSelector(".gridCelula.ng-binding");

	By btnRelatorio = By.id("botaoRelatorioCexats");
	By btnVisualizarRelatorio = By.cssSelector("#botaoVisualizarRelatorioCexats");
	
	
	By divRelatorio = By.xpath(".//*[@id='divVisualizarRelatorioCexats']/div");
	By btnFecharRelatorio = By.xpath(".//*[@id='divVisualizarRelatorioCexats']/div/form/fieldset/div[1]/a/img");
	
	By fecharPDF = By.cssSelector("#divVisualizarRelatorioCexatssAlteracoesNotasFiscais");
	By Testejp = By.className(
			"#divVisualizarRelatorioCexatssAlteracoesNotasFiscais > div > form > fieldset > div.tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble > a > img");
	By btnDesativarCexatss = By.id("botaoDesativarFiscais");
	By btnAtivarCexatss = By.id("botaoAtivarFiscais");

	WebDriverWait wait = new WebDriverWait(driver, 30);
	Actions actions = new Actions(driver);

	public CexatsPage clickBtnNovoCexats() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnNovoCexats));
		driver.findElement(btnNovoCexats).click();
		return this;
	}

	public CexatsPage localizarCexatsLotacao() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLocalizarLotacoes));
		driver.findElement(btnLocalizarLotacoes).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(lotacaoBusca));
		wait.until(ExpectedConditions.visibilityOfElementLocated(lotacaoBusca));
		driver.findElement(lotacaoBusca).sendKeys(lotacao);
		return this;
	}

	public CexatsPage cexats() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboCexats));
		WebElement element = driver.findElement(comboCexats);
		Select combo = new Select(element);
		combo.selectByVisibleText(valorCampoCexat);
		return this;
	}
	
	
	
	public CexatsPage uf() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboUf));
		WebElement element = driver.findElement(comboUf);
		Select combo = new Select(element);
		combo.selectByVisibleText(uf);
		return this;
	}
	
	public CexatsPage municipio() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboUf));
		WebElement element = driver.findElement(comboUf);
		Select combo = new Select(element);
		combo.selectByVisibleText(uf);
		return this;
	}
	
	

	public CexatsPage clickBtnPesquisarLotacao() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='tabelaComponenteLocalizarLotacoes']/tbody/tr[1]/td[1]")));
		WebElement element = driver
				.findElement(By.xpath(".//*[@id='tabelaComponenteLocalizarLotacoes']/tbody/tr[1]/td[1]"));
		actions.doubleClick(element).perform();
		Thread.sleep(1000);
		return this;
	}

	public CexatsPage localizarCexatsCidade() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLocalizarCidades));
		driver.findElement(btnLocalizarCidades).click();
		return this;
	}

	public CexatsPage clickBtnPesquisarCidade() throws InterruptedException {
		
//		wait.until(ExpectedConditions.visibilityOfElementLocated(nomeBusca));
//		driver.findElement(nomeBusca).click();
//		driver.findElement(nomeBusca).sendKeys(cidade);
//		
//		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
//		driver.findElement(nomeBusca).click();
//		driver.findElement(nomeBusca).sendKeys(cidade);
		
		WebElement element = driver
				.findElement(By.xpath(".//*[@id='tabelaComponenteLocalizarCidades']/tbody/tr[1]/td[1]"));
		Thread.sleep(1000);
		actions.doubleClick(element).perform();
		return this;
	}

	public CexatsPage clickBtnIncluirCexats() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirCexats));
		driver.findElement(btnIncluirCexats).click();
		return this;
	}

	public CexatsPage verificarAlertConfirmarInclusao() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluir);
			alert.accept();
		} catch (Exception e) {

		}
		return this;
	}

	public CexatsPage clickBtnLocalizarCexats() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(btnLocalizarCexats));
		Thread.sleep(1000);
		driver.findElement(btnLocalizarCexats).click();
		return this;
	}

	public CexatsPage realizarBuscaCexats() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoBuscaCexats));
		Thread.sleep(1000);
		driver.findElement(campoBuscaCexats).clear();
		Thread.sleep(1000);
		driver.findElement(campoBuscaCexats).sendKeys(cexat);
		return this;
	}

	public CexatsPage clickBtnPesquisarCexats() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(btnPesquisar).click();
		return this;
	}

	public CexatsPage clicKFecharLocalizarCexatss() {

		driver.findElement(btnFecharLocalizarCexatss).click();

		return this;
	}

	public CexatsPage clicKBtnAtualizarCexats() {
		driver.findElement(btnAtualizarCexats).click();
		return this;
	}

	public CexatsPage listaDeRegistrosEncontrados() throws InterruptedException {
		cexat = cexat.toUpperCase();
		Thread.sleep(2000);
		registro = driver.findElement(registrosEncontrados).getText().toUpperCase();

		return this;
	}

	public CexatsPage verificarAlertAtualizar() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAtualizacao);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public CexatsPage clickGridCexats() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(gridCexatss));
		driver.findElement(gridCexatss).click();
		return this;
	}

	public CexatsPage clickDesativar() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnDesativarCexatss));
		Thread.sleep(2000);
		driver.findElement(btnDesativarCexatss).click();

		return this;
	}

	public CexatsPage clickAtivar() throws InterruptedException{
	 wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtivarCexatss));
	 Thread.sleep(2000);
	 driver.findElement(btnAtivarCexatss).click();
	
	 return this;
	 }


	public CexatsPage verificarAlertExcluir() {
	 try {
	 wait.until(ExpectedConditions.alertIsPresent());
	 Alert alert = driver.switchTo().alert();
	 String textoAlert = alert.getText();
	 Assert.assertEquals(textoAlert, msgConfirmacaoExcluir);
	 alert.accept();
	 } catch (Exception e) {
	
	 }
	
	 return this;
	 }
	
	public CexatsPage perguntaAlertExcluir() {
		 try {
		 wait.until(ExpectedConditions.alertIsPresent());
		 Alert alert = driver.switchTo().alert();
		 String textoAlert = alert.getText();
		 Assert.assertEquals(textoAlert, msgConfirmarExcluir);
		 alert.accept();
		 } catch (Exception e) {
		
		 }
		
		 return this;
		 }


	
	 public CexatsPage clickBtnRelatorio()
	 throws InterruptedException {
	 wait.until(ExpectedConditions.elementToBeClickable(btnRelatorio));
	 Thread.sleep(2000);
	 driver.findElement(btnRelatorio).click();
	
	 return this;
	 }
	
	 public CexatsPage clickBtnVisualizarRelatorio()
	 throws InterruptedException {
	 wait.until(ExpectedConditions.elementToBeClickable(btnVisualizarRelatorio));
	 Thread.sleep(2000);
	 driver.findElement(btnVisualizarRelatorio).click();
	
	 return this;
	 }
	

	public CexatsPage clickBtnExcluir() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnExcluir));
		Thread.sleep(2000);
		driver.findElement(btnExcluir).click();

		return this;
	}

	public CexatsPage clickBtnInfCadastrais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnInfCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnInfCadastrais).click();

		return this;
	}

	public CexatsPage clickBtnFecharInfCadastrais() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharInformacoesCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnFecharInformacoesCadastrais).click();

		return this;
	}

	public CexatsPage clickBtnFecharInformacoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(btnFecharInformacoesCadastrais).click();
		return this;
	}

	public CexatsPage clickBtnFecharRelatorio() {
		
		driver.close();

		return this;
	}

	public CexatsPage clickBtnAtualizarCexats() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtualizarCexats));
		Thread.sleep(2000);
		driver.findElement(btnAtualizarCexats).click();
		return this;
	}

	public CexatsPage limparCexats() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLimparCampoCexats));
		Thread.sleep(1000);
		driver.findElement(btnLimparCampoCexats).click();
		Thread.sleep(1000);
		campoLimpo = driver.findElement(campoLotacao).getText().isEmpty();

		return this;
	}

	public CexatsPage informacoesCadastraisCexats() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(infoCadastrais));
		Thread.sleep(2000);
		exibirInfoCadastrais = driver.findElement(infoCadastrais).isDisplayed();

		return this;
	}

}
