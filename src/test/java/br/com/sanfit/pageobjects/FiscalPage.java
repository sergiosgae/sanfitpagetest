package br.com.sanfit.pageobjects;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class FiscalPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/cadastrar/fiscais/fiscais.jsp";

	public FiscalPage() {
		super(path);

	}

	public String registro;
	public boolean campoLimpo;
	public boolean exibirInfoCadastrais;
	public String fiscal = "ADELARDO GOMES MESQUITA LEITAO";
	String msgConfirmacaoIncluir = "Registro inclu�do com sucesso!";
	String msgConfirmacaoExcluir = "Registro exclu�do com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Registro atualizado com sucesso!";
	By btnNovoFiscal = By.id("botaoNovoFiscais");
	By btnLupaFiscal = By.id("botaoLupaLocalizarUsuarios");
	
	By btnIncluirFiscal = By.id("botaoIncluirFiscal");
	By campoFiscal = By.id("nomeBusca");
	By campoBuscaFiscal = By.id("nomeBusca");
	By btnLocalizarFiscal = By
			.id("botaoLocalizarFiscais");
	By btnPesquisar = By.id("botaoPesquisar");
	By btnExcluir = By.id("botaoExcluirFiscais");
	By btnAtualizarFiscal = By
			.id("botaoAtualizarFiscais");
	By btnLimparCampoFiscal = By.id("botaoLimparFiscais");
	By btnInfCadastrais = By.id("botaoCadastraisFiscais");
	By registrosEncontrados = By.cssSelector("#divExibirRegistrosFiscals");
	By infoCadastrais = By
			.cssSelector("#divInformacoesCadastraisFiscal");
	
	By gridCexatss = By.cssSelector(".gridCelula.ng-binding");
	By btnFecharInformacoesCadastrais = By.xpath(".//*[@id='divInformacoesCadastrais']/div/fieldset/div[1]/a/img");
	By btnFecharLocalizarFiscals = By
			.xpath(".//*[@id='formLocalizarFiscais']/div[1]/a/img");
//	By btnFecharInformacoesCadastrais = By
//			.cssSelector("#divInformacoesCadastraisFiscal");
	By gridFiscals = By
			.cssSelector(".gridCelula.ng-binding");
	
	By btnRelatorio = By.id("botaoRelatorioFiscais");
	By btnVisualizarRelatorio = By.xpath(".//*[@id='botaoVisualizarRelatorioFiscais']");
	By btnFecharRelatorio = By.xpath(".//*[@id='divVisualizarRelatorioFiscais']/div/form/fieldset/div[1]/a/img");			
	By fecharPDF = By.cssSelector("#divVisualizarRelatorioFiscal");
	By Testejp = By.className("#divVisualizarRelatorioFiscal > div > form > fieldset > div.tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble > a > img"); 
	By btnDesativarFiscals = By.id("botaoDesativarFiscais");
	By btnAtivarFiscals = By.id("botaoAtivarFiscais");
	By btnIncluirFiscais = By.id("botaoIncluirFiscais");
	
	WebDriverWait wait = new WebDriverWait(driver, 30);
	Actions actions = new Actions(driver);

	public FiscalPage clickBtnNovoFiscal() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnNovoFiscal));
		driver.findElement(btnNovoFiscal).click();
		return this;
	}
	
	
	public FiscalPage clickBtnLupaFiscal() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLupaFiscal));
		driver.findElement(btnLupaFiscal).click();
		return this;
	}
	

	
	public FiscalPage clickLupaIncluir() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLupaFiscal));
		driver.findElement(btnLupaFiscal).click();
		return this;
	}

	public FiscalPage preencherCampoFiscal() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoFiscal));
		driver.findElement(campoFiscal).clear();
		driver.findElement(campoFiscal).sendKeys(fiscal);

		return this;
	}
	
	
	public FiscalPage clickBtnPesquisar() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		return this;
	}
	
	
	public FiscalPage clickDuplo() throws InterruptedException {
		
		WebElement element = driver
				.findElement(By.xpath(".//*[@id='tabelaComponenteLocalizarUsuarios']/tbody/tr[1]/td[1]"));
		actions.doubleClick(element).perform();
		return this;
	}
	
		public FiscalPage clickGrid() throws InterruptedException {
		
			wait.until(ExpectedConditions.visibilityOfElementLocated(gridFiscals));
			driver.findElement(gridFiscals).click();
		return this;
	}
	

	public FiscalPage clickBtnIncluirFiscal() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnIncluirFiscais));
		driver.findElement(btnIncluirFiscais).click();

		return this;
	}

	
	
	public FiscalPage clickGridFiscal() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(gridFiscals));
		driver.findElement(gridFiscals).click();
		return this;
	}
	
	public FiscalPage clickDesativar() throws InterruptedException{
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnDesativarFiscals));
		Thread.sleep(2000);
		driver.findElement(btnDesativarFiscals).click();
		
		return this;
	}
	
	public FiscalPage clickAtivar() throws InterruptedException{
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtivarFiscals));
		Thread.sleep(2000);
		driver.findElement(btnAtivarFiscals).click();
		
		return this;
	}

	public FiscalPage verificarAlertIncluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public FiscalPage verificarAlertConfirmarExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmarExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public FiscalPage verificarAlertExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public FiscalPage verificarAlertAtualizar() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAtualizacao);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public FiscalPage clickBtnLocalizarFiscal()
			throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnLocalizarFiscal));
		driver.findElement(btnLocalizarFiscal).click();

		return this;
	}
	
	public FiscalPage clickBtnRelatorio()
			throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnRelatorio).click();

		return this;
	}
	
	public FiscalPage clickBtnVisualizarRelatorio()
			throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnVisualizarRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnVisualizarRelatorio).click();

		return this;
	}

	public FiscalPage clickBtnPesquisarFiscal() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		return this;
	}

	public FiscalPage clickBtnExcluir()
			throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnExcluir));
		Thread.sleep(2000);
		driver.findElement(btnExcluir).click();

		return this;
	}

	public FiscalPage clickBtnInfCadastrais()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnInfCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnInfCadastrais).click();

		return this;
	}
	
	public FiscalPage clickBtnFecharInfCadastrais()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnFecharInformacoesCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnFecharInformacoesCadastrais).click();

		return this;
	}
	

	public FiscalPage digitarFiscal() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(campoBuscaFiscal));
		driver.findElement(campoBuscaFiscal).clear();
		driver.findElement(campoBuscaFiscal).sendKeys(fiscal);

		return this;
	}

	public FiscalPage clicKFecharLocalizarFiscals() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnFecharLocalizarFiscals));
		driver.findElement(btnFecharLocalizarFiscals).click();

		return this;
	}

	public FiscalPage clickBtnFecharInformacoesFiscais() {
		driver.findElement(btnFecharInformacoesCadastrais).click();
		return this;
	}
	
	public FiscalPage clickBtnFecharRelatorio() {
		driver.findElement(btnFecharRelatorio).isDisplayed();
		return this;
	}
	
	public FiscalPage listaDeRegistrosEncontrados()
			throws InterruptedException {
		fiscal = fiscal.toUpperCase();
		Thread.sleep(2000);
		registro = driver.findElement(registrosEncontrados).getText()
				.toUpperCase();

		return this;
	}

	public FiscalPage clickBtnAtualizarFiscal()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnAtualizarFiscal));
		Thread.sleep(2000);
		driver.findElement(btnAtualizarFiscal).click();

		return this;
	}

	public FiscalPage limparFiscal()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(btnLimparCampoFiscal));
		Thread.sleep(2000);
		driver.findElement(btnLimparCampoFiscal).click();
		campoLimpo = driver.findElement(campoBuscaFiscal).getText().isEmpty();

		return this;
	}

	public FiscalPage informacoesCadastraisFiscal()
			throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(infoCadastrais));
		Thread.sleep(2000);
		exibirInfoCadastrais = driver.findElement(infoCadastrais).isDisplayed();

		return this;
	}

}
