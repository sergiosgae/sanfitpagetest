package br.com.sanfit.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

	public static String path = "index.jsp";
	WebDriverWait wait = new WebDriverWait(driver, 30);

	
    public LoginPage() {
		super(path);
		
	}

	By usernameLocator = By.id("login");
	By passwordLocator = By.id("senha");
	By passwordLocatorDepoisDoClick = By.id("senha");
	By loginButtonLocator = By.id("botaoEntrar");

	public LoginPage typeUsername(String login) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(usernameLocator));
		driver.findElement(usernameLocator).sendKeys(login);
		return this;
	}

	public LoginPage typePassword(String senha) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(passwordLocator));
		driver.findElement(passwordLocator).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(passwordLocatorDepoisDoClick));
		driver.findElement(passwordLocatorDepoisDoClick).sendKeys(senha);
		
		return this;
	}

	public HomePage submitLogin() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(loginButtonLocator));
		driver.findElement(loginButtonLocator).submit();
		HomePage homePage = new HomePage();
		navegarParaOutraPagina(homePage);
		//driver.navigate().to(ConfigUtils.getServerUrl()  +"subsistemas/subsistemaAlteracaoNotasFiscais/index.jsp");
		return homePage;
	}

	public HomePage loginAs(String login, String senha) {
		typeUsername(login);
		typePassword(senha);
		return submitLogin();
	}
	

	public void submitLoginerro(String login, String senhaLabel) {
		typeUsername(login);
		typePassword(senhaLabel);
		driver.findElement(loginButtonLocator).submit();
		
	}
}
