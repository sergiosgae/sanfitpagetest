package br.com.sanfit.pageobjects;

import java.util.Random;
import java.util.Set;

import javax.swing.JFrame;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PedidosPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/cadastrar/pedidos/pedidos.jsp?cnpj=0";

	public PedidosPage() {
		super(path);

	}

	public String registro;
	public String tipoProcesso = "ICMS / NOTA FISCAL - SELAGEM";
	Random aleatorio = new Random();
	
	
	public String descricao = "teste" + numeroAleatorio();
	public String descricaoPendecias = "teste" + numeroAleatorio();
	public String frame = "frameAnexarAnexos";
	public String obsNotasFiscais = "Teste obs notas fiscaisTesteTeste obs notas fiscaisTesteTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscais";
	public String opcaoDePesquisa = "CHAVE DE ACESSO";
	public String motivo = "EMISS�O DE CARTA DE CORRE��O";
	public String chaveDeAcesso = "35171201502510000391580010000217971000217977";
	public boolean campoLimpo;
	public boolean exibirInfoCadastrais;
	public String pedido = "Testes Para Verificar";
	String msgConfirmacaoIncluir = "Registro inclu�do com sucesso!";
	String msgSolicitarCancelamento = "Solicita��o de cancelamento efetuada com sucesso! Aguarde an�lise do fiscal.";
	String msgMsgPendeciaPedidos = "Caro Contribuinte,existem pend�ncias a serem solucionadas para dar andamento ao seus processos.Para acessa-las clique no Menu lateral na op��o 'Minhas pend�ncias'.";

	String msgConfirmacaoIncluirAnexo = "Anexo inclu�do com sucesso!";
	String msgConfirmacaoExcluir = "Registro exclu�do com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Registro atualizado com sucesso!";
	String msgNotaIncluida = "Nota fiscal incluida com sucesso!";
	String msgNotaIncluidaOutroPedido = "Esta nota fiscal n�o pode ser incluida em um novo pedido! J� se encontra no processo N�O 7680865/2017 - AGUARDANDO DISTRIBUI��O.";
	public String file = "src/test/resources/config/teste.pdf";

	By btnNovoPedido = By.id("botaoNovoPedidos");
	By btnLupaPedido = By
			.xpath(".//*[@id='divExibirRegistrosTabelaResultadoPendentes']/table/tbody/tr[1]/td[1]/img[1]");
	By abaCancelamento = By
			.xpath(".//*[@id='divExibirRegistrosTabelaResultadoPendentes']/table/tbody/tr[1]/td[1]/img[1]");
	
	By abaCancelamentoProcesso = By
			.xpath(".//*[@id='abaCancelamento']/a");
	
	By btnSolicitarCancelamento = By.id("botaoSolicitarCancelamento");

	By btnImprimirHistorico = By.cssSelector("#botaoImprimirProtocolo");

	By btnIncluirNotaFiscal = By.id("botaoPesquisarNotasFiscais");
	By btnIncluirAnexo = By.id("botaoPesquisarAnexo");

	By btnIncluirFinalizarAnexo = By.id("botaoIncluirAnexo");
	By btnResponderPendencia = By.id("botaoSolucionarPendenciaInferior");
	By menuMinhasPendencias = By.xpath(".//*[@id='lateralMenu']/div/div[2]/ul/li[3]");
	By btnEnviarPedido = By.id("botaoEnviarPedido");
	By btnIncluirNotaFiscais = By.id("botaoPesquisarNotasFiscais");
	By botaoNotaFiscais = By.id("botaoIncluirNota");
	By btnFecharDetalhesNota = By.xpath(".//*[@id='tituloInclusao']/a/img");
	By btnFecharPesquisaNotaFiscal = By.xpath(".//*[@id='fecharDivPesquisarNotasFiscais']/img");
	By btnFecharImprimir = By.cssSelector("#fecharDivProtocolo>img");

	By campoPedido = By.id("pedido");
	By campoChaveDeAcesso = By.id("chaveAcesso");
	By campoDescricao = By.id("descricaoAnexo");
	By campoRespostaDaPendencia = By.id("respostaPendencias");
	By campoMotivoCancelamento = By.id("motivoCancelamento");
	By campoSelo = By.id("numeroSelo");
	By obsNotas = By.id("observacoesNota");

	By campoBuscaPedido = By.id("pedidoBusca");
	By caminho = By.cssSelector("#caminho");
	By btnLocalizarPedido = By.id("botaoLocalizarPedidos");
	By btnPesquisar = By.xpath(".//*[@id='divAlinhamentoEsquerdoHintPesquisarNotas']/div[2]/a");
	By btnMaisIncluirNota = By.cssSelector("#tabelaListaNotasFiscais>tbody>tr>td>img");
	By btnExcluir = By.id("botaoExcluirPedidos");
	By btnAtualizarPedido = By.id("botaoAtualizarPedidos");
	By comboTipoProcesso = By.xpath(".//*[@id='divFundoBrandoListaConteudoAmpliado']/div[3]/div/select");
	By comboOpcaoDePesquisa = By.id("codigoOpcaoPesquisa");
	By comboMotivo = By.id("codigoMotivo");
	By btnLimparCampoPedido = By.id("botaoLimparPedidos");
	By btnInfCadastrais = By.id("botaoCadastraisPedidos");
	By registrosEncontrados = By.cssSelector("#divExibirRegistrosPedidos");
	By infoCadastrais = By.cssSelector("#divInformacoesCadastraisPedidos");
	By btnFecharInformacoesCadastrais = By
			.xpath(".//*[@id='divInformacoesCadastraisPedidos']/div/fieldset/div[1]/a/img");
	By btnFecharLocalizarPedidos = By.cssSelector("#fecharDivLocalizar>img");
	// By btnFecharInformacoesCadastrais = By
	// .cssSelector("#divInformacoesCadastraisPedidos");
	By gridPedidos = By.cssSelector(".gridCelula.ng-binding");

	By btnRelatorio = By.id("botaoRelatorioPedidos");
	By btnVisualizarRelatorio = By.cssSelector("#botaoVisualizarRelatorioPedidos");
	By btnFecharRelatorio = By.cssSelector("#divVisualizarRelatorioPedidos");
	By fecharPDF = By.cssSelector("#divVisualizarRelatorioPedidos");
	By Testejp = By.className(
			"#divVisualizarRelatorioPedidos > div > form > fieldset > div.tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble > a > img");
	By btnDesativarPedidos = By.id("botaoDesativarFiscais");
	By btnAtivarPedidos = By.id("botaoAtivarFiscais");

	WebDriverWait wait = new WebDriverWait(driver, 30);

	public PedidosPage clickBtnNovoPedido() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnNovoPedido));
		driver.findElement(btnNovoPedido).click();

		return this;
	}

	public PedidosPage preencherCampoPedido() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoPedido));
		driver.findElement(campoPedido).clear();
		driver.findElement(campoPedido).sendKeys(pedido);

		return this;
	}

	public PedidosPage comboTipoProcesso(String tipo) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboTipoProcesso));
		WebElement element = driver.findElement(comboTipoProcesso);
		Select combo = new Select(element);
		combo.selectByVisibleText(tipo);
		return this;
	}

	public PedidosPage comboOpcaoDePesquisa(String tipo) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboOpcaoDePesquisa));
		WebElement element = driver.findElement(comboOpcaoDePesquisa);
		Select combo = new Select(element);
		combo.selectByVisibleText(tipo);
		return this;
	}

	public PedidosPage digitarChaveDeAcesso(String chave) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoChaveDeAcesso));
		Thread.sleep(2000);
		driver.findElement(campoChaveDeAcesso).sendKeys(chave);
		return this;
	}

	public PedidosPage digitarNumeroSelo(String selo) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoSelo));
		Thread.sleep(2000);
		driver.findElement(campoSelo).sendKeys(selo);
		return this;
	}

	public PedidosPage clickBtnPesquisar() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		return this;
	}

	public PedidosPage clickMaisIncluirNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnMaisIncluirNota));
		driver.findElement(btnMaisIncluirNota).click();
		return this;
	}

	public PedidosPage comboMotivo() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboMotivo));
		WebElement element = driver.findElement(comboMotivo);
		Select combo = new Select(element);
		combo.selectByVisibleText(motivo);
		return this;
	}

	public PedidosPage digitarObservacoes() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(obsNotas));
		Thread.sleep(2000);
		driver.findElement(obsNotas).sendKeys(obsNotasFiscais);
		return this;
	}

	public PedidosPage clickBtnIncluirNotaFiscal() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirNotaFiscais));
		driver.findElement(btnIncluirNotaFiscais).click();
		return this;
	}

	public PedidosPage clickBtnIncluir() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(botaoNotaFiscais));
		driver.findElement(botaoNotaFiscais).click();
		return this;
	}

	public PedidosPage clickBtnFecharDetalhesNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharPesquisaNotaFiscal));
		driver.findElement(btnFecharDetalhesNota).click();
		return this;

	}

	public PedidosPage clickBtnFecharImprimir() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharImprimir));
		driver.findElement(btnFecharImprimir).click();
		return this;

	}

	public PedidosPage clickBtnFecharPesquisaNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharPesquisaNotaFiscal));
		driver.findElement(btnFecharPesquisaNotaFiscal).click();
		return this;

	}

	public PedidosPage clickBtnIncluirAnexo() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirAnexo));
		driver.findElement(btnIncluirAnexo).click();
		return this;
	}

	public PedidosPage anexando() throws InterruptedException {
		driver.switchTo().frame(frame);
		wait.until(ExpectedConditions.visibilityOfElementLocated(caminho));
		driver.findElement(caminho).sendKeys(file);
		driver.switchTo().defaultContent();
		return this;
	}

	public PedidosPage digitarDescricao() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoDescricao));
		Thread.sleep(2000);
		descricao = "descricaoAnexo"+numeroAleatorio();
		driver.findElement(campoDescricao).sendKeys(descricao);
		descricao="";
		return this;
	}
	
	public PedidosPage digitarDescricaoPendencia() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoDescricao));
		Thread.sleep(2000);
		descricaoPendecias = "pendencia"+ numeroPendencia();
		driver.findElement(campoDescricao).sendKeys(descricaoPendecias);
		descricaoPendecias = "";
		return this;
	}

	public PedidosPage digitarRespostaDaPendencia() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoRespostaDaPendencia));
		Thread.sleep(2000);
		descricao = "testeRespondendoDescricao";
		driver.findElement(campoRespostaDaPendencia).sendKeys(descricao);
		return this;
	}

	public PedidosPage clickBtnFinalizarAnexo() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirFinalizarAnexo));
		driver.findElement(btnIncluirFinalizarAnexo).click();
		
		return this;
	}

	public PedidosPage clickBtnResponderPendencia() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnResponderPendencia));
		driver.findElement(btnResponderPendencia).click();
		return this;
	}

	public PedidosPage clickMenuMinhasPendencias() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(menuMinhasPendencias));
		driver.findElement(menuMinhasPendencias).click();
		return this;
	}

	public PedidosPage clickLupaProcesso() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLupaPedido));
		driver.findElement(btnLupaPedido).click();
		return this;
	}

	public PedidosPage clickBtnImprimirHistorico() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnImprimirHistorico));
		driver.findElement(btnImprimirHistorico).click();
		return this;
	}

	public PedidosPage clickAbaCancelamento() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(abaCancelamento));
		driver.findElement(abaCancelamento).click();
		return this;
	}
	
	public PedidosPage clickAbaCancelamentoProcesso() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(abaCancelamentoProcesso));
		Thread.sleep(1000);
		driver.findElement(abaCancelamentoProcesso).click();
		Thread.sleep(1000);
		return this;
	}

	public PedidosPage clickBtnSolicitarCancelamento() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnSolicitarCancelamento));
		Thread.sleep(1000);
		driver.findElement(btnSolicitarCancelamento).click();
		Thread.sleep(1000);
		return this;
	}

	public PedidosPage digitarMotivoCancelamento() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(campoMotivoCancelamento));
		Thread.sleep(2000);
		descricao = "MotivoCancelamento";
		driver.findElement(campoMotivoCancelamento).sendKeys(descricao);
		return this;
	}

	public PedidosPage clickBtnEnviarPedido() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnEnviarPedido));
		driver.findElement(btnEnviarPedido).click();
		return this;
	}

	public PedidosPage verificarAlertIncluir() throws InterruptedException {
		Thread.sleep(2000);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}
	
	public PedidosPage verificarAlerSecaoExpirada() throws InterruptedException {
		Thread.sleep(2000);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}
	
	
	public PedidosPage verificarAlertSolicitarCancelamento() throws InterruptedException {
		Thread.sleep(2000);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgSolicitarCancelamento);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public String numeroProcesso() {
		String numeroProcesso = driver.findElement(By.cssSelector("#numeroProcessoProtocolo")).getText();

		return numeroProcesso;
	}

	public PedidosPage verificarAlertIncluirAnexo() {

		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluirAnexo);
			alert.accept();
			numeroProcesso();
		} catch (Exception e) {

		}

		return this;
	}

	public PedidosPage verificarAlertNotaIncluida() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgNotaIncluida);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PedidosPage verificarAlertNotaIncluidaNotaOutroPedido() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgNotaIncluidaOutroPedido);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PedidosPage verificarAlertConfirmarExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmarExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PedidosPage verificarAlertExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public Integer numeroAleatorio() {

		Random gerador = new Random(1000);

		for (int i = 0; i < 10; i++) {
			System.out.println(gerador.nextInt());
		}

		int aleatorio = gerador.nextInt();
		return aleatorio;
	}
	
	public Integer numeroPendencia() {

		Random gerador = new Random(25);
		int aleatorio = gerador.nextInt();
		return aleatorio;
	}

	public PedidosPage verificarAlertAtualizar() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAtualizacao);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PedidosPage clickBtnLocalizarPedido() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnLocalizarPedido));
		Thread.sleep(2000);
		driver.findElement(btnLocalizarPedido).click();

		return this;
	}

	public PedidosPage clicKFecharLocalizarPedidos() {

		driver.findElement(btnFecharLocalizarPedidos).click();

		return this;
	}

	public PedidosPage clickBtnFecharInformacoesFiscais() {
		driver.findElement(btnFecharInformacoesCadastrais).click();
		return this;
	}

	public PedidosPage clickBtnFecharRelatorio() {
		driver.findElement(btnFecharRelatorio).isDisplayed();

		return this;
	}

	public PedidosPage listaDeRegistrosEncontrados() throws InterruptedException {
		pedido = pedido.toUpperCase();
		Thread.sleep(2000);
		registro = driver.findElement(registrosEncontrados).getText().toUpperCase();

		return this;
	}

	public PedidosPage verificaMsgrAlertPedidoComPendencia() throws InterruptedException {
		Thread.sleep(2000);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PedidosPage verificaMsgrAlerPendenciaPrazo10Dias() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

}
