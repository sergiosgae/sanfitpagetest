package br.com.sanfit.pageobjects;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PostosFiscaisPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/cadastrar/postosFiscais/postosFiscais.jsp";

	public PostosFiscaisPage() {
		super(path);

	}

	public String registro;
	public boolean campoLimpo;
	public boolean exibirInfoCadastrais;

	String msgConfirmacaoIncluir = "Registro incluído com sucesso!";
	String msgConfirmacaoExcluir = "Registro excluído com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Registro atualizado com sucesso!";
	By btnLupaLotacoes = By.id("botaoLocalizarLotacoes");
	By btnLupaCidade = By.id("botaoLocalizarCidades");
	By btnLocalizarLotacoes = By.id("botaoLocalizarLotacoes");
	By lotacaoBusca = By.id("lotacaoBusca");
	public String lotacao = "ASSESSORIA JURIDICA";
	By btnPesquisar = By.id("botaoPesquisar");
	By gridPostosFiscais = By.cssSelector(".gridCelula.ng-binding");

	By btnLocalizarCidades = By.id("botaoLocalizarCidades");

	By btnNovoPostoFiscal = By.id("botaoNovoPostosFiscais");
	By nomeBusca = By.className("ng-pristine ng-valid ng-empty ng-valid-maxlength ng-touched");
	public String cidade = "ABADIA DE GOIÁS";
	public String cexat = "NUCLEO DE ATENDIMENTO E MONITORAMENTO EM CAUCAIA";

	By btnIncluirPostosFiscais = By.id("botaoIncluirPostosFiscais");
	By comboPostosFiscais = By.id("codigoCexat");
	String valorCampoCexat = "LOTAÇÃO INICIAL";

	By campoBuscaPostosFiscais = By.id("nomeBusca");
	By campoLotacao = By.id("lotacao");

	By btnLocalizarPostosFiscais = By.id("botaoLocalizarPostosFiscais");

	By btnExcluir = By.id("botaoExcluirPostosFiscais");
	By btnAtualizarPostosFiscais = By.id("botaoAtualizarPostosFiscais");
	By btnLimparCampoPostosFiscais = By.id("botaoLimparPostosFiscais");
	By btnInfCadastrais = By.id("botaoCadastraisPostosFiscais");
	By registrosEncontrados = By.cssSelector("#divExibirRegistrosPostosFiscais");
	By infoCadastrais = By.cssSelector("#divInformacoesCadastraisPostosFiscaissAlteracoesNotasFiscais");
	By btnFecharInformacoesCadastrais = By.xpath(".//*[@id='divInformacoesCadastrais']/div/fieldset/div[1]/a/img");
	By btnFecharLocalizarPostosFiscaiss = By.cssSelector(".tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble>a>img");
	By btnFecharLotacoes = By.cssSelector("#btnFecharDivLocalizarArvoresAssuntos>img");
	By gridPostosFiscaiss = By.cssSelector(".gridCelula.ng-binding");

	By btnRelatorio = By.id("botaoRelatorioPostosFiscais");
	By btnVisualizarRelatorio = By.cssSelector("#botaoVisualizarRelatorioPostosFiscais");

	By divRelatorio = By.xpath(".//*[@id='divVisualizarRelatorioPostosFiscais']/div");
	By btnFecharRelatorio = By.xpath(".//*[@id='divVisualizarRelatorioPostosFiscais']/div/form/fieldset/div[1]/a/img");

	By fecharPDF = By.cssSelector("#divVisualizarRelatorioPostosFiscaissAlteracoesNotasFiscais");
	By Testejp = By.className(
			"#divVisualizarRelatorioPostosFiscaissAlteracoesNotasFiscais > div > form > fieldset > div.tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble > a > img");
	By btnDesativarPostosFiscaiss = By.id("botaoDesativarFiscais");
	By btnAtivarPostosFiscaiss = By.id("botaoAtivarFiscais");

	WebDriverWait wait = new WebDriverWait(driver, 30);
	Actions actions = new Actions(driver);

	public PostosFiscaisPage clickBtnNovoPostosFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnNovoPostoFiscal));
		Thread.sleep(1000);
		driver.findElement(btnNovoPostoFiscal).click();
		Thread.sleep(1000);
		return this;
	}

	public PostosFiscaisPage clickBtnLupaPostosFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLupaLotacoes));
		Thread.sleep(1000);
		driver.findElement(btnLupaLotacoes).click();
		return this;
	}

	public PostosFiscaisPage localizarCexatsCidade() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLocalizarCidades));
		driver.findElement(btnLocalizarCidades).click();
		return this;
	}

	public PostosFiscaisPage clickBtnPesquisarCidade() throws InterruptedException {

		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLupaCidade));
		Thread.sleep(1000);
		driver.findElement(btnLupaCidade).click();
		// wait.until(ExpectedConditions.visibilityOfElementLocated(nomeBusca));
		// driver.findElement(nomeBusca).sendKeys(cidade);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		// driver.findElement(nomeBusca).click();
		// driver.findElement(nomeBusca).sendKeys(cidade);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='usuario-164']/td[1]")));
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath(".//*[@id='usuario-164']/td[1]"));
		Thread.sleep(1000);
		actions.doubleClick(element).perform();
		return this;
	}

	public PostosFiscaisPage digitarLotacao() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(lotacaoBusca));
		Thread.sleep(1000);
		driver.findElement(lotacaoBusca).sendKeys(lotacao);
		return this;
	}

	public PostosFiscaisPage clickDuplo() throws InterruptedException {
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath(".//*[@id='lotacao-1']/td"));
		Thread.sleep(1000);
		actions.doubleClick(element).perform();
		return this;
	}

	public PostosFiscaisPage SelecionaCexatsCombo() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboPostosFiscais));
		Thread.sleep(1000);
		WebElement element = driver.findElement(comboPostosFiscais);
		Thread.sleep(1000);
		Select combo = new Select(element);
		combo.selectByVisibleText(valorCampoCexat);
		Thread.sleep(1000);
		return this;
	}

	public PostosFiscaisPage clickBtnPesquisarLotacao() throws InterruptedException {

		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		return this;
	}

	public PostosFiscaisPage localizarPostosFiscaisCidade() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLocalizarCidades));
		driver.findElement(btnLocalizarCidades).click();
		return this;
	}

	public PostosFiscaisPage clickBtnIncluirPostosFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirPostosFiscais));
		driver.findElement(btnIncluirPostosFiscais).click();
		return this;
	}

	public PostosFiscaisPage verificarAlertConfirmarInclusao() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluir);
			alert.accept();
		} catch (Exception e) {

		}
		return this;
	}

	public PostosFiscaisPage clickBtnLocalizarPostosFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(btnLocalizarPostosFiscais));
		Thread.sleep(1000);
		driver.findElement(btnLocalizarPostosFiscais).click();
		return this;
	}

	public PostosFiscaisPage realizarBuscaPostosFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoBuscaPostosFiscais));
		Thread.sleep(1000);
		driver.findElement(campoBuscaPostosFiscais).clear();
		Thread.sleep(1000);
		driver.findElement(campoBuscaPostosFiscais).sendKeys(cexat);
		return this;
	}

	public PostosFiscaisPage clickBtnPesquisarPostosFiscais() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(btnPesquisar).click();
		return this;
	}

	public PostosFiscaisPage clicKFecharLocalizarPostosFiscaiss() {

		driver.findElement(btnFecharLocalizarPostosFiscaiss).click();

		return this;
	}

	public PostosFiscaisPage clicKBtnAtualizarPostosFiscais() {
		driver.findElement(btnAtualizarPostosFiscais).click();
		return this;
	}

	public PostosFiscaisPage listaDeRegistrosEncontrados() throws InterruptedException {
		cexat = cexat.toUpperCase();
		Thread.sleep(2000);
		registro = driver.findElement(registrosEncontrados).getText().toUpperCase();

		return this;
	}

	public PostosFiscaisPage verificarAlertAtualizar() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAtualizacao);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PostosFiscaisPage clickGridPostosFiscais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(gridPostosFiscaiss));
		Thread.sleep(1000);
		driver.findElement(gridPostosFiscaiss).click();
		return this;
	}

	public PostosFiscaisPage clickDesativar() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnDesativarPostosFiscaiss));
		Thread.sleep(1000);
		driver.findElement(btnDesativarPostosFiscaiss).click();

		return this;
	}

	public PostosFiscaisPage clickAtivar() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtivarPostosFiscaiss));
		Thread.sleep(1000);
		driver.findElement(btnAtivarPostosFiscaiss).click();

		return this;
	}

	public PostosFiscaisPage verificarAlertExcluir() {
		try {
			Thread.sleep(1000);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PostosFiscaisPage perguntaAlertExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmarExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public PostosFiscaisPage clickBtnRelatorio() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnRelatorio).click();

		return this;
	}

	public PostosFiscaisPage clickBtnVisualizarRelatorio() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnVisualizarRelatorio));
		Thread.sleep(2000);
		driver.findElement(btnVisualizarRelatorio).click();

		return this;
	}

	public PostosFiscaisPage clickBtnExcluir() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnExcluir));
		Thread.sleep(1000);

		driver.findElement(btnExcluir).click();

		return this;
	}

	public PostosFiscaisPage clickBtnInfCadastrais() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnInfCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnInfCadastrais).click();

		return this;
	}

	public PostosFiscaisPage clickBtnFecharInfCadastrais() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharInformacoesCadastrais));
		Thread.sleep(2000);
		driver.findElement(btnFecharInformacoesCadastrais).click();

		return this;
	}

	public PostosFiscaisPage clickBtnFecharInformacoesFiscais() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(btnFecharInformacoesCadastrais).click();
		return this;
	}

	public PostosFiscaisPage clickBtnFecharRelatorio() {

		driver.close();

		return this;
	}

	public PostosFiscaisPage clickBtnAtualizarPostosFiscais() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAtualizarPostosFiscais));
		Thread.sleep(2000);
		driver.findElement(btnAtualizarPostosFiscais).click();
		return this;
	}

	public PostosFiscaisPage limparPostosFiscais() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLimparCampoPostosFiscais));
		Thread.sleep(1000);
		driver.findElement(btnLimparCampoPostosFiscais).click();
		Thread.sleep(1000);
		campoLimpo = driver.findElement(campoLotacao).getText().isEmpty();

		return this;
	}

	public PostosFiscaisPage informacoesCadastraisPostosFiscais() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(infoCadastrais));
		Thread.sleep(2000);
		exibirInfoCadastrais = driver.findElement(infoCadastrais).isDisplayed();

		return this;
	}

}
