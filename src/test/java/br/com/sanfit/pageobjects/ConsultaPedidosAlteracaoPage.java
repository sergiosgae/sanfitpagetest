package br.com.sanfit.pageobjects;

import java.util.Random;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ConsultaPedidosAlteracaoPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/consultar/listaPedidos/listaPedidos.jsp";

	public ConsultaPedidosAlteracaoPage() {
		super(path);

	}

	public String registro;
	public String tipoProcesso = "ICMS / NOTA FISCAL - SELAGEM";
	Random aleatorio = new Random();
	
	public String frame = "frameAnexarAnexos";
	public String obsNotasFiscais = "Teste obs notas fiscaisTesteTeste obs notas fiscaisTesteTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscais";
	public String opcaoDePesquisa = "CHAVE DE ACESSO";
	
	String msgConfirmacaoIncluirAnexo = "Anexo inclu�do com sucesso!";
	String msgConfirmacaoExcluir = "Registro exclu�do com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Filtro aplicado com sucesso em todas as prateleiras!";
	String msgNotaIncluida = "Nota fiscal incluida com sucesso!";
	String msgNotaIncluidaOutroPedido = "Esta nota fiscal n�o pode ser incluida em um novo pedido! J� se encontra no processo N�O 7680865/2017 - AGUARDANDO DISTRIBUI��O.";
	public String file = "C:\\Users\\casa\\git\\sanfitpagetest\\src\\test\\resources\\config\\teste32323.pdf";

	By numeroProcesso = By.id("numeroProcessoPesquisa");
	By checkMarcarProcessoPlatilheira = By.xpath(".//*[@id='divExibirRegistrosConsultaPedido3']/table/tbody/tr[1]/td[8]/input");

	By btnPesquisar = By.id("botaoPesquisar");
	By btnAceitarCancelamento = By.xpath(".//*[@id='gridPedidosSolicitacaoCancelamento']/.//*[@id='btnAceitarSolicitacaoCancelamento']");
	By abaPedidosPendenciaAguardandoAnalise = By.xpath(".//*[@id='prateleiraPedidosSolucionadosAguardandoAnalise']");
	By abaPedidosPendenciaCancelamento = By.xpath(".//*[@id='prateleiraPedidosSolicitacaoCancelamento']/tbody/tr/td[3]");
	By lupa = By.xpath(".//*[@id='divExibirRegistrosConsultaPedido2']/table/tbody/tr[1]/td[1]/img[1]");
	

	WebDriverWait wait = new WebDriverWait(driver, 30);
	
	
	public ConsultaPedidosAlteracaoPage digitaNumeroProcesso(String numero) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(numeroProcesso));
		driver.findElement(numeroProcesso).click();
		driver.findElement(numeroProcesso).sendKeys(numero);
		return this;
	}
	
	
	public ConsultaPedidosAlteracaoPage clickBotaoPesquisar() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		return this;
	}
	
	public ConsultaPedidosAlteracaoPage clickAbaPedidoPendenciaAguardandoAnalise() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(abaPedidosPendenciaAguardandoAnalise));
		driver.findElement(abaPedidosPendenciaAguardandoAnalise).click();
		return this;
	}
	
	public ConsultaPedidosAlteracaoPage clickAbaPedidoPendenciaAguardandoCancelamento() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(abaPedidosPendenciaCancelamento));
		driver.findElement(abaPedidosPendenciaCancelamento).click();
		return this;
	}
	
	
	
	public ConsultaPedidosAlteracaoPage clickLupa() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(lupa));
		Thread.sleep(2000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		driver.findElement(lupa).click();
		Thread.sleep(2000);
		return this;
	}
	
	
	public ConsultaPedidosAlteracaoPage clickMarcarProcessoPlatilheira() throws InterruptedException {
		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,300)", "");
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkMarcarProcessoPlatilheira));
		driver.findElement(checkMarcarProcessoPlatilheira).click();
		return this;
	}
	
	public ConsultaPedidosAlteracaoPage clickBtnAceitarCancelamento() throws InterruptedException {
		Thread.sleep(1000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(btnAceitarCancelamento));
		driver.findElement(btnAceitarCancelamento).click();
		return this;
	}
	
	
	
	
	public ConsultaPedidosAlteracaoPage verificarAlertConsulta() throws InterruptedException {
		Thread.sleep(2000);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}



	

}
