package br.com.sanfit.pageobjects;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class ConsultaPedidosPage extends BasePage {

	public static String path = "subsistemas/subsistemaAlteracaoNotasFiscais/consultar/pedidos/consultarPedidos.jsp";

	public ConsultaPedidosPage() {
		super(path);

	}

	public String registro;
	public String tipoProcesso = "ICMS / NOTA FISCAL - SELAGEM";
	public String descricao = "teste descricao";
	public String motivoDeferirNota = "teste motivo ";
	public String frame = "frameAnexarAnexos";
	public String frameFinalizarPedido = "frameAnexarInformacaoFiscal";
	public String obsNotasFiscais = "Teste obs notas fiscaisTesteTeste obs notas fiscaisTesteTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscaisTeste obs notas fiscais";
	public String opcaoDePesquisa = "CHAVE DE ACESSO";
	public String motivo = "CANCELAMENTO DE SELO: NÃO OCORREU A CIRCULAÇÃO DA MERCADORIA";
	public String fiscal = "ALAN DE SOUSA BARROSO";
	
	public String chaveDeAcesso = "35171201502510000391580010000217971000217977";
	public boolean campoLimpo;
	public boolean exibirInfoCadastrais;
	public String pedido = "Testes Para Verificar";
	String msgConfirmacaoIncluir = "Registro inclu�do com sucesso!";
	String msgConfirmacaoExcluir = "Registro exclu�do com sucesso!";
	String msgConfirmarExcluir = "Deseja excluir este registro?";
	String msgConfirmacaoAtualizacao = "Registro atualizado com sucesso!";
	String msgMsgPendeciaPedidos = "Caro Contribuinte,existem pend�ncias a serem solucionadas para dar andamento ao seus processos.Para acessá-las clique no Menu lateral na opção 'Minhas pendências'.";
	String msgConfirmacaoAlteracaoFiscal = "Deseja alterar o respons�vel pelo processo?";
	String msgNotaIncluida = "Nota fiscal incluida com sucesso!";
	String msgNotaIncluidaOutroPedido = "Esta nota fiscal não pode ser incluida em um novo pedido! Já se encontra no processo Nº 7680865/2017 - AGUARDANDO DISTRIBUIÇÃO.";
	public String file = "C:\\Users\\t_sergioluis\\Documents\\teste.pdf";
	public static String assuntoICMSNotaFiscalCorrecao="ICMS / NOTA FISCAL - CORRECAO";
	public static String assuntoICMSNotaFiscalSelagem="ICMS / NOTA FISCAL - SELAGEM";
	public static String[] numerosProceessos={"7681241/2017"};


	By btnNovoPedido = By.id("botaoNovoPedidos");
	By btnIncluirNotaFiscal = By.id("botaoPesquisarNotasFiscais");
	By btnIncluirAnexo = By.id("botaoPesquisarAnexo");
	
	By btnIncluirFinalizarAnexo = By.id("botaoIncluirAnexo");
	By btnEnviarPedido = By.id("botaoEnviarPedido");
	By btnIncluirNotaFiscais = By.id("botaoPesquisarNotasFiscais");
	By botaoNotaFiscais = By.id("botaoIncluirNota");
	By btnFecharDetalhesNota = By.xpath(".//*[@id='tituloInclusao']/a/img");
	By btnFecharPesquisaNotaFiscal = By.xpath(".//*[@id='fecharDivPesquisarNotasFiscais']/img");
	
	
	By campoPedido = By.id("pedido");
	By campoChaveDeAcesso = By.id("numeroProcessoPesquisa");
	By campoDescricao = By.id("descricaoAnexo");
	By campoSelo = By.id("numeroSelo");
	By campoMotivoDeferirNota = By.id("motivoDeferirNotaFiscal");
	By campoMotivodIneferirNota = By.id("motivoDeferirNotaFiscal");
	By campoPendencia = By.id("pendencia");
	
	By campoMotivoDeferirParcialmenteNota = By.id("motivoIndeferirNotaFiscal");
	By campoResumoPendecias = By.id("resumoEnvioPendencias");
	By obsNotas = By.id("observacoesNota");

	By campoBuscaPedido = By.id("pedidoBusca");
	By caminho = By.cssSelector("#caminho");
	By btnLocalizarPedido = By.id("botaoLocalizarPedidos");
	By btnPesquisar = By.id("botaoPesquisar");
	By btnAceitarCancelamento = By.id("botaoPesquisar");
	By btnLupaProcesso = By.xpath("/html/body/div[2]/div[3]/form/div/div/div/div/div[3]/div/div[5]/div/div/div/div[1]/div[2]/table/tbody/tr[1]/td[1]/img[1]");
	By checkMarcarProcesso = By.cssSelector("#divExibirRegistrosNotasFiscais>table>tbody>tr>td>input");
	By checkMarcarProcessoPlatilheira = By.xpath(".//*[@id='divExibirRegistrosConsultaPedido3']/table/tbody/tr[1]/td[8]/input");

	By clickDesfazer = By.xpath(".//*[@id='divExibirRegistrosNotasFiscais']/table/tbody/tr/td[10]/img");
	By clickDetalhesNota = By.cssSelector("#divExibirRegistrosNotasFiscais>table>tbody>tr>td>img");

	By btnDedefirNota = By.id("botaoDeferirNota");
	By btnIndedefirNota = By.id("botaoDeferirNota");
	By btnGravarPendencias = By.id("botaoGravarPendencia");
	By btnFinalizarPedido = By.id("botaoFinalizarPedido");
	By btnConfirmar = By.xpath(".//*[@id='divAlinhamentoEsquerdoLocalizarDocumento']/.//*[@id='botaoConfirmar']");
	By btnDedefirParcialmenteNota = By.id("botaoDeferirNotaParcialmente");
	
	
	By btnConfirmarDedefirNota = By.id("btnConfirmarDeferimentoNotaFiscal");
	By btnConfirmarIndedefirNota = By.id("btnConfirmarDeferimentoNotaFiscal");
	By btnConfirmarDeferirParciamenteNota = By.id("btnConfirmarIndeferimentoNotaFiscal");
	By btnConfirmarGravarPendencias = By.id("btnGravarPendenciaNota");
	By btnEnvarPendencias = By.id("botaoEnviarPendencias");
	By btnConfirmarEnviarPendencias = By.xpath(".//*[@id='divEnviarPendencias']/.//*[@id='botaoEnviarPendencias']");


	By checkMarcarProcessoAtribuirFiscal = By.cssSelector("#divExibirRegistrosConsultaPedido-1>table>tbody>tr>td>input");

	By btnExcluir = By.id("botaoExcluirPedidos");
	By btnAtualizarPedido = By.id("botaoAtualizarPedidos");
	By comboTipoProcesso = By.xpath(".//*[@id='divFundoBrandoListaConteudoAmpliado']/div[3]/div/select");
	By comboOpcaoDePesquisa = By.name("codigoAssuntoSanfit");
	By comboMotivo = By.id("codigoMotivo");
	By comboFiscal = By.id("codigoFiscalAtribuicao");
	By btnLimparCampoPedido = By.id("botaoLimparPedidos");
	By btnInfCadastrais = By.id("botaoCadastraisPedidos");
	By registrosEncontrados = By.cssSelector("#divExibirRegistrosPedidos");
	By infoCadastrais = By.cssSelector("#divInformacoesCadastraisPedidos");
	By btnFecharInformacoesCadastrais = By
			.xpath(".//*[@id='divInformacoesCadastraisPedidos']/div/fieldset/div[1]/a/img");
	By btnFecharLocalizarPedidos = By.cssSelector("#fecharDivLocalizar>img");
	// By btnFecharInformacoesCadastrais = By
	// .cssSelector("#divInformacoesCadastraisPedidos");
	By gridPedidos = By.cssSelector(".gridCelula.ng-binding");

	By btnRelatorio = By.id("botaoRelatorioPedidos");
	By btnVisualizarRelatorio = By.cssSelector("#botaoVisualizarRelatorioPedidos");
	By btnFecharRelatorio = By.cssSelector("#divVisualizarRelatorioPedidos");
	By fecharPDF = By.cssSelector("#divVisualizarRelatorioPedidos");
	By Testejp = By.className(
			"#divVisualizarRelatorioPedidos > div > form > fieldset > div.tituloFormularioDivBusca.divBuscaLinhaBaixo.dragAble > a > img");
	By btnDesativarPedidos = By.id("botaoDesativarFiscais");
	By btnAtivarPedidos = By.id("botaoAtivarFiscais");

	WebDriverWait wait = new WebDriverWait(driver, 30);

	public ConsultaPedidosPage clickBtnNovoPedido() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnNovoPedido));
		driver.findElement(btnNovoPedido).click();

		return this;
	}

	public ConsultaPedidosPage preencherCampoPedido() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(campoPedido));
		driver.findElement(campoPedido).clear();
		driver.findElement(campoPedido).sendKeys(pedido);

		return this;
	}

	public ConsultaPedidosPage comboTipoProcesso(String tipo) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboTipoProcesso));
		WebElement element = driver.findElement(comboTipoProcesso);
		Select combo = new Select(element);
		combo.selectByVisibleText(tipo);
		return this;
	}

	public ConsultaPedidosPage comboOpcaoDePesquisa(String tipo) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboOpcaoDePesquisa));
		WebElement element = driver.findElement(comboOpcaoDePesquisa);
		Select combo = new Select(element);
		combo.selectByVisibleText(tipo);
		return this;
	}

	public ConsultaPedidosPage digitarNumeroProcesso(String chave) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoChaveDeAcesso));
		Thread.sleep(2000);
		driver.findElement(campoChaveDeAcesso).sendKeys(chave);
		Thread.sleep(2000);
		return this;
	}

	public ConsultaPedidosPage digitarNumeroSelo(String selo) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoSelo));
		Thread.sleep(1000);
		driver.findElement(campoSelo).sendKeys(selo);
		return this;
	}

	public ConsultaPedidosPage clickBtnPesquisar() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnPesquisar));
		driver.findElement(btnPesquisar).click();
		Thread.sleep(2000);
		
		return this;
	}

	public ConsultaPedidosPage clickLupaProcesso() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnLupaProcesso));
		driver.findElement(btnLupaProcesso).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnAceitarCancelamento() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnAceitarCancelamento));
		driver.findElement(btnAceitarCancelamento).click();
		return this;
	}
	
	public ConsultaPedidosPage clickMarcarProcesso() throws InterruptedException {
		Thread.sleep(3000);
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkMarcarProcesso));
		
		
		driver.findElement(checkMarcarProcesso).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickMarcarProcessoPlatilheira() throws InterruptedException {
		Thread.sleep(3000);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkMarcarProcesso));
		
		
		driver.findElement(checkMarcarProcesso).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickDetalhesNota() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clickDetalhesNota));
		driver.findElement(clickDetalhesNota).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickDesfazer() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clickDesfazer));
		driver.findElement(clickDesfazer).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnDeferirNota() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnDedefirNota));
		driver.findElement(btnDedefirNota).click();
		return this;
	}
	
	public ConsultaPedidosPage clickBtnDeferirParcialmenteNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnDedefirParcialmenteNota));
		driver.findElement(btnDedefirParcialmenteNota).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnIndeferirNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIndedefirNota));
		driver.findElement(btnIndedefirNota).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnGravarPendencias() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnGravarPendencias));
		driver.findElement(btnGravarPendencias).click();
		return this;
	}
	
	public ConsultaPedidosPage clickBtnFinalizarPedido() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFinalizarPedido));
		driver.findElement(btnFinalizarPedido).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnConfirmar() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnConfirmar));
		driver.findElement(btnConfirmar).click();
		return this;
	}
	
	public ConsultaPedidosPage digitarMotivoDeferirNota() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoMotivoDeferirNota));
		Thread.sleep(2000);
		driver.findElement(campoMotivoDeferirNota).sendKeys(motivoDeferirNota);
		return this;
	}
	
	
	public ConsultaPedidosPage digitarMotivoIndeferirNota() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoMotivodIneferirNota));
		Thread.sleep(2000);
		driver.findElement(campoMotivodIneferirNota).sendKeys(motivoDeferirNota);
		return this;
	}
	
	
	public ConsultaPedidosPage digitarGravarPendencias() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoPendencia));
		Thread.sleep(2000);
		driver.findElement(campoPendencia).sendKeys(motivoDeferirNota);
		return this;
	}
	

	public ConsultaPedidosPage digitarMotivoDeferirParcialmenteNota() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoMotivoDeferirParcialmenteNota));
		Thread.sleep(1000);
		driver.findElement(campoMotivoDeferirParcialmenteNota).sendKeys(motivoDeferirNota);
		return this;
	}
	
	
	public ConsultaPedidosPage digitarResumoPendencias() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoResumoPendecias));
		Thread.sleep(2000);
		driver.findElement(campoResumoPendecias).sendKeys(motivoDeferirNota);
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnConfirmarMotivoDeferirNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnConfirmarDedefirNota));
		driver.findElement(btnConfirmarDedefirNota).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnConfirmarMotivoIndeferirNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnConfirmarIndedefirNota));
		driver.findElement(btnConfirmarIndedefirNota).click();
		return this;
	}
	
	
	
	public ConsultaPedidosPage clickBtnConfirmarMotivoDeferirParcialmenteNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnConfirmarDeferirParciamenteNota));
		driver.findElement(btnConfirmarDeferirParciamenteNota).click();
		return this;
	}
	
	public ConsultaPedidosPage clickBtnConfirmarGravarPendencias() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnConfirmarGravarPendencias));
		driver.findElement(btnConfirmarGravarPendencias).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnEnviarPendencias() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnEnvarPendencias));
		driver.findElement(btnEnvarPendencias).click();
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnConfirmarEnviarPendencias() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnConfirmarEnviarPendencias));
		driver.findElement(btnConfirmarEnviarPendencias).click();
		return this;
	}
	
	
	
	
	public ConsultaPedidosPage clickMarcarProcessoParaAtribuirFiscal() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkMarcarProcessoAtribuirFiscal));
		driver.findElement(checkMarcarProcessoAtribuirFiscal).click();
		return this;
	}


	public ConsultaPedidosPage comboMotivo() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboMotivo));
		WebElement element = driver.findElement(comboMotivo);
		Select combo = new Select(element);
		combo.selectByVisibleText(motivo);
		return this;
	}
	
	public ConsultaPedidosPage comboSelecionaFiscal() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(comboFiscal));
		WebElement element = driver.findElement(comboFiscal);
		Select combo = new Select(element);
		combo.selectByVisibleText(fiscal);
		return this;
	}

	public ConsultaPedidosPage digitarObservacoes() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(obsNotas));
		Thread.sleep(2000);
		driver.findElement(obsNotas).sendKeys(obsNotasFiscais);
		return this;
	}

	public ConsultaPedidosPage clickBtnIncluirNotaFiscal() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirNotaFiscais));
		driver.findElement(btnIncluirNotaFiscais).click();
		return this;
	}

	public ConsultaPedidosPage clickBtnIncluir() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(botaoNotaFiscais));
		driver.findElement(botaoNotaFiscais).click();
		return this;
	}

	public ConsultaPedidosPage clickBtnFecharDetalhesNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharDetalhesNota));
		driver.findElement(btnFecharDetalhesNota).click();
		return this;

	}
	
	public ConsultaPedidosPage clickBtnFecharPesquisaNota() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnFecharPesquisaNotaFiscal));
		driver.findElement(btnFecharPesquisaNotaFiscal).click();
		return this;

	}

	public ConsultaPedidosPage clickBtnIncluirAnexo() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirAnexo));
		driver.findElement(btnIncluirAnexo).click();
		return this;
	}

	public ConsultaPedidosPage anexando() throws InterruptedException {
		Thread.sleep(1000);
		driver.switchTo().frame(frame);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(caminho));
		driver.findElement(caminho).click();
		driver.findElement(caminho).sendKeys(file);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		return this;
	}
	
	public ConsultaPedidosPage anexandoFinalizarPedido() throws InterruptedException {
		Thread.sleep(1000);
		driver.switchTo().frame(frameFinalizarPedido);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(caminho));
		driver.findElement(caminho).click();
		driver.findElement(caminho).sendKeys(file);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		return this;
	}
	
	
	
	public ConsultaPedidosPage digitarDescricao() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(campoDescricao));
		Thread.sleep(2000);
		driver.findElement(campoDescricao).sendKeys(descricao);
		return this;
	}
	
	
	public ConsultaPedidosPage clickBtnFinalizarAnexo() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnIncluirFinalizarAnexo));
		driver.findElement(btnIncluirFinalizarAnexo).click();
		return this;
	}

	public ConsultaPedidosPage clickBtnEnviarPedido() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(btnEnviarPedido));
		driver.findElement(btnEnviarPedido).click();
		return this;
	}

	public ConsultaPedidosPage verificarAlertIncluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoIncluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	
	public ConsultaPedidosPage clicaOK() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}
	
	public ConsultaPedidosPage verificarAlertNotaIncluida() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgNotaIncluida);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}
	
	
	public ConsultaPedidosPage verificarAlertNotaIncluidaNotaOutroPedido() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgNotaIncluidaOutroPedido);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public ConsultaPedidosPage verificarAlertConfirmarExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmarExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public ConsultaPedidosPage verificarAlertExcluir() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoExcluir);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}

	public ConsultaPedidosPage verificarAlertAtualizar() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAtualizacao);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}
	
	public ConsultaPedidosPage verificaMsgrAlertPedidoComPendencia() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgMsgPendeciaPedidos);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}
	
	
	
	public ConsultaPedidosPage verificarAlertConfirmacaoFiscal() {
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			String textoAlert = alert.getText();
			Assert.assertEquals(textoAlert, msgConfirmacaoAlteracaoFiscal);
			alert.accept();
		} catch (Exception e) {

		}

		return this;
	}


	public ConsultaPedidosPage clickBtnLocalizarPedido() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(btnLocalizarPedido));
		Thread.sleep(2000);
		driver.findElement(btnLocalizarPedido).click();

		return this;
	}

	public ConsultaPedidosPage clicKFecharLocalizarPedidos() {

		driver.findElement(btnFecharLocalizarPedidos).click();

		return this;
	}

	public ConsultaPedidosPage clickBtnFecharInformacoesFiscais() {
		driver.findElement(btnFecharInformacoesCadastrais).click();
		return this;
	}

	public ConsultaPedidosPage clickBtnFecharRelatorio() {
		driver.findElement(btnFecharRelatorio).isDisplayed();

		return this;
	}

	public ConsultaPedidosPage listaDeRegistrosEncontrados() throws InterruptedException {
		pedido = pedido.toUpperCase();
		Thread.sleep(2000);
		registro = driver.findElement(registrosEncontrados).getText().toUpperCase();

		return this;
	}

}
