package br.com.sanfit.steps;

import org.testng.annotations.AfterSuite;

import br.com.sanfit.setup.SeleniumDriverFactory;

public class BaseTeste {
	
	 @AfterSuite
	   public void tearDown() throws Exception {
	        SeleniumDriverFactory.getDriver().quit();
	 
	   }

}
