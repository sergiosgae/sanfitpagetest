package br.com.sanfit.steps;

import org.testng.Assert;
import org.testng.annotations.Test;
import br.com.sanfit.pageobjects.LoginPage;
import br.com.sanfit.pageobjects.PedidosPage;
import br.com.sanfit.pageobjects.ConsultaPedidosPage;

public class ConsultaPedidosTestes extends BaseTeste {
	ConsultaPedidosPage consultapedido = new ConsultaPedidosPage();
	LoginPage loginPage = new LoginPage();
	PedidosPage pedido = new PedidosPage();
	
	

	
	@Test(priority=1)
	public void fazer_CosultarPedido() throws InterruptedException {
		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		consultapedido.navegarParaOutraPagina(consultapedido);
		consultapedido.comboOpcaoDePesquisa(ConsultaPedidosPage.assuntoICMSNotaFiscalCorrecao);
		consultapedido.digitarNumeroProcesso(ConsultaPedidosPage.numerosProceessos[0]);
		consultapedido.clickBtnPesquisar();
//		consultapedido.clickMarcarProcessoParaAtribuirFiscal().comboSelecionaFiscal().verificarAlertConfirmacaoFiscal().verificarAlertAtualizar();
//		consultapedido.verificarAlertIncluir();
		//Deferindp
		consultapedido.clickLupaProcesso().clickMarcarProcesso().clickBtnDeferirNota().digitarMotivoDeferirNota().clickBtnConfirmarMotivoDeferirNota();
		consultapedido.verificarAlertAtualizar().clickMarcarProcesso().clickDesfazer().verificarAlertAtualizar().clickMarcarProcesso();
		//Deferindo parcialmente
		consultapedido.clickBtnDeferirParcialmenteNota().digitarMotivoDeferirParcialmenteNota().clickBtnConfirmarMotivoDeferirParcialmenteNota()
		.verificarAlertAtualizar().clickDesfazer().verificarAlertAtualizar();
		//Indeferindo
		consultapedido.clickMarcarProcesso().clickBtnIndeferirNota()
		.digitarMotivoIndeferirNota().clickBtnConfirmarMotivoIndeferirNota()
		.verificarAlertAtualizar().clickDesfazer().verificarAlertAtualizar()
		//Gravando Pendencias
		.clickMarcarProcesso().clickBtnGravarPendencias().digitarGravarPendencias().clickBtnConfirmarGravarPendencias()
		.verificarAlertIncluir()
		//Enviando Pendências
		.clickBtnEnviarPendencias().clickBtnEnviarPendencias().verificarAlertIncluir();
		pedido.navegarParaOutraPagina(pedido);
		pedido.verificaMsgrAlertPedidoComPendencia();
		
			
	}  
	
	

}
