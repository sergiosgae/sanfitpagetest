package br.com.sanfit.steps;

import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.CexatsPage;
import br.com.sanfit.pageobjects.LoginPage;

public class CexatsTestes extends BaseTeste {
	CexatsPage cexats = new CexatsPage();
	LoginPage loginPage = new LoginPage();

	@Test(priority = 1)
	public void fazer_CadastrarCexats() throws InterruptedException {
		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		cexats.navegarParaOutraPagina(cexats);
		cexats.clickBtnNovoCexats().localizarCexatsLotacao().clickBtnPesquisarLotacao();
		cexats.localizarCexatsCidade().clickBtnPesquisarCidade();
		cexats.clickBtnIncluirCexats().verificarAlertConfirmarInclusao();

	}

	@Test(priority = 2)
	public void fazer_ExcluirCexats() throws InterruptedException {
		cexats.clickGridCexats();
		cexats.clickBtnExcluir();
		cexats.perguntaAlertExcluir().verificarAlertExcluir();

	}

	@Test(priority = 2)
	public void fazer_AtualizarCexats() throws InterruptedException {
		cexats.clickGridCexats();
		cexats.clickBtnAtualizarCexats();
		cexats.verificarAlertAtualizar();
	}

	@Test(priority = 3)
	public void fazer_LimparCexats() throws InterruptedException {
		cexats.clickGridCexats().limparCexats();

	}

	@Test(priority = 4)
	public void fazer_ExibirInformacoesCadastrais() throws InterruptedException {
		cexats.clickGridCexats();
		cexats.clickBtnInfCadastrais();
		cexats.clickBtnFecharInformacoesFiscais();

	}

	@Test(priority = 6)
	public void fazer_VisualizarRelatorio() throws InterruptedException {
		cexats.clickGridCexats();
		cexats.clickBtnRelatorio();
		cexats.clickBtnVisualizarRelatorio();
		cexats.clickBtnFecharRelatorio();
	}
	
	
	@Test(priority = 7)
	public void fazer_Associar() throws InterruptedException {
		cexats.clickGridCexats();
		cexats.clickBtnRelatorio();
		cexats.clickBtnVisualizarRelatorio();
		cexats.clickBtnFecharRelatorio();
	}

	@Test(priority = 5)
	public void fazer_CexatsDesativarAtivar() throws InterruptedException {

		cexats.clickGridCexats();
		cexats.clickDesativar();
		cexats.verificarAlertAtualizar();
		cexats.clickGridCexats();
		cexats.clickAtivar();
		cexats.verificarAlertAtualizar();
	}
}
