package br.com.sanfit.steps;

import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.PlantoesFiscaisPage;
import br.com.sanfit.pageobjects.LoginPage;

public class PlantoesFiscaisTestes extends BaseTeste {
	PlantoesFiscaisPage postofiscal = new PlantoesFiscaisPage();
	LoginPage loginPage = new LoginPage();

	@Test(priority = 1)
	public void fazer_CadastrarPlantoesFiscais() throws InterruptedException {
		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		postofiscal.navegarParaOutraPagina(postofiscal);
		postofiscal.clickBtnLupaFiscal().digitarFiscal().checkMarcarFiscal().clickBtnIncluirFiscal();
	}

	@Test(priority = 7)
	public void fazer_ExcluirPlantoesFiscais() throws InterruptedException {
		postofiscal.clickGridPlantoesFiscais();
		postofiscal.clickBtnExcluir();
		postofiscal.perguntaAlertExcluir().verificarAlertExcluir();

	}

	@Test(priority = 2)
	public void fazer_AtualizarPlantoesFiscais() throws InterruptedException {
		postofiscal.clickGridPlantoesFiscais();
		postofiscal.clickBtnAtualizarPlantoesFiscais();
		postofiscal.verificarAlertAtualizar();
	}

	@Test(priority = 3)
	public void fazer_LimparPlantoesFiscais() throws InterruptedException {
		postofiscal.clickGridPlantoesFiscais().limparPlantoesFiscais();

	}

	@Test(priority = 4)
	public void fazer_ExibirInformacoesCadastrais() throws InterruptedException {
		postofiscal.clickGridPlantoesFiscais();
		postofiscal.clickBtnInfCadastrais();
		postofiscal.clickBtnFecharInformacoesFiscais();

	}

	@Test(priority = 5)
	public void fazer_VisualizarRelatorio() throws InterruptedException {
		postofiscal.clickGridPlantoesFiscais();
		postofiscal.clickBtnRelatorio();
		postofiscal.clickBtnVisualizarRelatorio();
		postofiscal.clickBtnFecharRelatorio();
	}
	
	 @Test(priority=6)
	 public void fazer_PlantoesFiscaisDesativarAtivar() throws InterruptedException{

	 postofiscal.clickGridPlantoesFiscais();
	 postofiscal.clickDesativar();
	 postofiscal.verificarAlertAtualizar();
	 postofiscal.clickGridPlantoesFiscais();
	 postofiscal.clickAtivar();
	 postofiscal.verificarAlertAtualizar();
	 }
}
