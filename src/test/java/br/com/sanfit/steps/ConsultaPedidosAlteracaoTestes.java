package br.com.sanfit.steps;

import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.ConsultaPedidosAlteracaoPage;
import br.com.sanfit.pageobjects.ConsultaPedidosPage;
import br.com.sanfit.pageobjects.LoginPage;
import br.com.sanfit.pageobjects.PedidosPage;

public class ConsultaPedidosAlteracaoTestes extends BaseTeste {
	PedidosPage pedido = new PedidosPage();
	ConsultaPedidosAlteracaoPage consultaPedidoAlteracao = new ConsultaPedidosAlteracaoPage();
	ConsultaPedidosPage consultapedido = new ConsultaPedidosPage();
	LoginPage loginPage = new LoginPage();
	String nprocesso;
	
	
	
	@Test(priority = 1)
	public void fazer_ResponderPendencias() throws InterruptedException {
		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		pedido.navegarParaOutraPagina(consultaPedidoAlteracao);
		consultaPedidoAlteracao.digitaNumeroProcesso("0001094/2018");
		consultaPedidoAlteracao.clickBotaoPesquisar().verificarAlertConsulta().clickAbaPedidoPendenciaAguardandoAnalise();
		consultaPedidoAlteracao.clickLupa();
		consultapedido.clickMarcarProcesso().clickBtnDeferirNota();
		consultapedido.digitarMotivoDeferirNota().clickBtnConfirmarMotivoDeferirNota().verificarAlertAtualizar()
				.clickMarcarProcesso();
		consultapedido.clickBtnFinalizarPedido().clickBtnConfirmar().verificarAlertAtualizar();
	}

	

}
