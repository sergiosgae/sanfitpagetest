package br.com.sanfit.steps;

import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.PostosFiscaisPage;
import br.com.sanfit.pageobjects.LoginPage;

public class PostosFiscaisTestes extends BaseTeste {
	PostosFiscaisPage postofiscal = new PostosFiscaisPage();
	LoginPage loginPage = new LoginPage();

	@Test(priority = 1)
	public void fazer_CadastrarPostosFiscais() throws InterruptedException {
		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		postofiscal.navegarParaOutraPagina(postofiscal);
		postofiscal.clickBtnNovoPostosFiscais().clickBtnLupaPostosFiscais().digitarLotacao().clickGridPostosFiscais().clickDuplo();
		postofiscal.SelecionaCexatsCombo().clickBtnPesquisarCidade();
		postofiscal.clickBtnIncluirPostosFiscais().verificarAlertConfirmarInclusao();
	}

	@Test(priority = 7)
	public void fazer_ExcluirPostosFiscais() throws InterruptedException {
		postofiscal.clickGridPostosFiscais();
		postofiscal.clickBtnExcluir();
		postofiscal.perguntaAlertExcluir().verificarAlertExcluir();

	}

	@Test(priority = 2)
	public void fazer_AtualizarPostosFiscais() throws InterruptedException {
		postofiscal.clickGridPostosFiscais();
		postofiscal.clickBtnAtualizarPostosFiscais();
		postofiscal.verificarAlertAtualizar();
	}

	@Test(priority = 3)
	public void fazer_LimparPostosFiscais() throws InterruptedException {
		postofiscal.clickGridPostosFiscais().limparPostosFiscais();

	}

	@Test(priority = 4)
	public void fazer_ExibirInformacoesCadastrais() throws InterruptedException {
		postofiscal.clickGridPostosFiscais();
		postofiscal.clickBtnInfCadastrais();
		postofiscal.clickBtnFecharInformacoesFiscais();

	}

	@Test(priority = 5)
	public void fazer_VisualizarRelatorio() throws InterruptedException {
		postofiscal.clickGridPostosFiscais();
		postofiscal.clickBtnRelatorio();
		postofiscal.clickBtnVisualizarRelatorio();
		postofiscal.clickBtnFecharRelatorio();
	}
	
	 @Test(priority=6)
	 public void fazer_PostosFiscaisDesativarAtivar() throws InterruptedException{

	 postofiscal.clickGridPostosFiscais();
	 postofiscal.clickDesativar();
	 postofiscal.verificarAlertAtualizar();
	 postofiscal.clickGridPostosFiscais();
	 postofiscal.clickAtivar();
	 postofiscal.verificarAlertAtualizar();
	 }
}
