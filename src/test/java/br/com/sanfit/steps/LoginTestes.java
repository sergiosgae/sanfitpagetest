package br.com.sanfit.steps;



import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.BasePage;
import br.com.sanfit.pageobjects.LoginPage;


public class LoginTestes extends BaseTeste{
    LoginPage loginPage = new LoginPage();
    //teste
    @Test
    public void fazer_login(){
    	loginPage.navegarPara();
    	loginPage.loginAs("master", "teste");
    	
    }
   
   @Test
   public void usuario_nao_cadastrado(){
	   loginPage.navegarPara();
	   loginPage.submitLoginerro("teste","teste");
	   String alerta = BasePage.driver.switchTo().alert().getText();
	   Assert.assertEquals("Usuário não existe na base de dados do SISSEG!", alerta);
	   Alert alert = BasePage.driver.switchTo().alert();
	   alert.accept();
   }
   
   @Test
   public void senha_invalida(){
	   loginPage.navegarPara();
	   loginPage.submitLoginerro("master","");
	   String alerta = BasePage.driver.switchTo().alert().getText();
	   Assert.assertEquals("Login ou senha inválidos!", alerta);
	   Alert alert = BasePage.driver.switchTo().alert();
	   alert.accept();
   }
}
