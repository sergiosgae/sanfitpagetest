package br.com.sanfit.steps;

import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.LoginPage;
import br.com.sanfit.pageobjects.MotivosAlteracoesNotasFiscaisPage;

public class MotivosAlteracoesNotasFiscaisTestes extends BaseTeste {
	MotivosAlteracoesNotasFiscaisPage motivos = new MotivosAlteracoesNotasFiscaisPage();
	LoginPage loginPage = new LoginPage();

	@Test(priority=1)
	public void fazer_CadastrarMotivosDeAlteracoesDeNotasFiscais() {
		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		motivos.navegarParaOutraPagina(motivos);
 		motivos.clickBtnNovoMotivo().preencherCampoMotivo().clickBtnIncluirMotivo().verificarAlertIncluir();	
	}
	
	@Test(priority=2)
	public void fazer_LocalizarMotivo() throws InterruptedException{
		motivos.clickBtnLocalizarMotivo();
		motivos.realizarBuscaMotivo();
		motivos.clickBtnPesquisarMotivo();
		motivos.listaDeRegistrosEncontrados();
		motivos.clicKFecharLocalizarMotivos();
	//	Assert.assertTrue(motivos.registro.contains(motivos.motivo));
	}
	
	@Test(priority=7)
	public void fazer_ExcluirMotivo() throws InterruptedException{
		motivos.clickBtnLocalizarMotivo();
		motivos.realizarBuscaMotivo();
		motivos.clickBtnPesquisarMotivo();
		motivos.clicKFecharLocalizarMotivos();
		motivos.listaDeRegistrosEncontrados();
		motivos.clickGridMotivo();
		motivos.clickBtnExcluir();
		motivos.verificarAlertConfirmarExcluir();
		motivos.verificarAlertExcluir();		
	}
	
	@Test(priority=3)
	public void fazer_AtualizarMotivo() throws InterruptedException{
		motivos.clickBtnLocalizarMotivo();
		motivos.realizarBuscaMotivo();
		motivos.clickBtnPesquisarMotivo();
		motivos.clicKFecharLocalizarMotivos();
		motivos.listaDeRegistrosEncontrados();
		motivos.clickBtnAtualizarMotivo();
		motivos.verificarAlertAtualizar();		
	}
	
	@Test(priority=8)
	public void fazer_LimparMotivo() throws InterruptedException{
		motivos.clickBtnLocalizarMotivo();
		motivos.realizarBuscaMotivo();
		motivos.clickBtnPesquisarMotivo();
		motivos.limparMotivo();		
		Assert.assertTrue(motivos.campoLimpo);
	}
	
	@Test(priority=4)
	public void fazer_ExibirInformacoesCadastrais() throws InterruptedException{
		motivos.clickBtnLocalizarMotivo();
		motivos.realizarBuscaMotivo();
		motivos.clickBtnPesquisarMotivo();
		motivos.clicKFecharLocalizarMotivos();
		motivos.listaDeRegistrosEncontrados();
		motivos.clickGridMotivo();
		motivos.clickBtnInfCadastrais();
		motivos.informacoesCadastraisMotivo();		
		Assert.assertTrue(motivos.exibirInfoCadastrais);
		motivos.clickBtnFecharInformacoesFiscais();
		
	}
//	
//	@Test(priority=6)
//	public void fazer_VisualizarRelatorio() throws InterruptedException{
//		motivos.clickGridMotivo();
//		motivos.clickBtnRelatorio();
//		motivos.clickBtnVisualizarRelatorio();
//		motivos.clickBtnFecharRelatorio();
//	}
//	
	@Test(priority=5)
	public void fazer_MotivoDesativarAtivar() throws InterruptedException{
		motivos.clickBtnLocalizarMotivo();
		motivos.realizarBuscaMotivo();
		motivos.clickBtnPesquisarMotivo();
		motivos.clicKFecharLocalizarMotivos();
		motivos.listaDeRegistrosEncontrados();
		motivos.clickGridMotivo();
		motivos.clickDesativar();
		motivos.verificarAlertAtualizar();
		motivos.clickBtnLocalizarMotivo();
		motivos.realizarBuscaMotivo();
		motivos.clickBtnPesquisarMotivo();
		motivos.clicKFecharLocalizarMotivos();
		motivos.listaDeRegistrosEncontrados();
		motivos.clickGridMotivo();
		
		motivos.clickAtivar();
		motivos.verificarAlertAtualizar();
	}
}
