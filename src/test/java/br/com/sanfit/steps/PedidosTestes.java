package br.com.sanfit.steps;

import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.ConsultaPedidosAlteracaoPage;
import br.com.sanfit.pageobjects.ConsultaPedidosPage;
import br.com.sanfit.pageobjects.LoginPage;
import br.com.sanfit.pageobjects.PedidosPage;

public class PedidosTestes extends BaseTeste {
	PedidosPage pedido = new PedidosPage();
	ConsultaPedidosPage consultapedido = new ConsultaPedidosPage();
	ConsultaPedidosAlteracaoPage consultaPedidoAlteracao = new ConsultaPedidosAlteracaoPage();
	LoginPage loginPage = new LoginPage();
	String nprocesso;

	 @Test(priority = 1)
	 public void
	 fazer_CadastrarPedidosIcmsCorrecaoComSeloDeferindoFinalizando() throws
	 InterruptedException {
	 loginPage.navegarPara();
	 loginPage.loginAs("master", "teste");
	 pedido.navegarParaOutraPagina(pedido);
	 pedido.verificarAlerSecaoExpirada();
	 pedido.comboTipoProcesso("ICMS / NOTA FISCAL - CORRECAO");
	 pedido.clickBtnIncluirNotaFiscal();
	 pedido.comboOpcaoDePesquisa("Nº do selo");
	 pedido.digitarNumeroSelo("201600069174");
	 pedido.clickBtnPesquisar().clickMaisIncluirNota().comboMotivo();
	 pedido.digitarObservacoes().clickBtnIncluir().verificarAlertNotaIncluida().clickBtnFecharPesquisaNota();
	 pedido.clickBtnIncluirAnexo().anexando().digitarDescricao().clickBtnFinalizarAnexo()
	 .verificarAlertIncluirAnexo();
	 pedido.clickBtnEnviarPedido().verificarAlertIncluir();
	 nprocesso = pedido.numeroProcesso();
	 System.out.println(nprocesso);
	 //pedido.clickBtnImprimirHistorico();
	 pedido.clickBtnFecharImprimir();
	 loginPage.navegarPara();
	 loginPage.loginAs("master", "teste");
	 consultapedido.navegarParaOutraPagina(consultapedido);
	 pedido.verificarAlerSecaoExpirada();
	 consultapedido.comboOpcaoDePesquisa(ConsultaPedidosPage.assuntoICMSNotaFiscalCorrecao);
	 consultapedido.digitarNumeroProcesso(nprocesso);
	 consultapedido.clickBtnPesquisar();
	 consultapedido.clickLupaProcesso().clickMarcarProcesso().clickBtnDeferirNota();
	 consultapedido.digitarMotivoDeferirNota().clickBtnConfirmarMotivoDeferirNota().verificarAlertAtualizar().clickMarcarProcesso();
	 consultapedido.clickBtnFinalizarPedido().clickBtnConfirmar().verificarAlertAtualizar();
	
	 }

	 @Test(priority = 2)
	 public void
	 fazer_CadatrarPedidoAtribuindoFiscalDeferindoDeferindoPGravandoPendenciasRespondendoPendenciasFinalizando()
	 throws InterruptedException {
	 loginPage.navegarPara();
	 loginPage.loginAs("master", "teste");
	 pedido.navegarParaOutraPagina(pedido);
	 pedido.comboTipoProcesso("ICMS / NOTA FISCAL - CORRECAO");
	 pedido.clickBtnIncluirNotaFiscal();
	 pedido.comboOpcaoDePesquisa("Nº do selo");
	 pedido.digitarNumeroSelo("201600068909");
	 pedido.clickBtnPesquisar().clickMaisIncluirNota().comboMotivo();
	 pedido.digitarObservacoes().clickBtnIncluir().verificarAlertNotaIncluida().clickBtnFecharPesquisaNota();
	 pedido.clickBtnIncluirAnexo().anexando().digitarDescricao().clickBtnFinalizarAnexo()
	 .verificarAlertIncluirAnexo();
	 pedido.clickBtnEnviarPedido().verificarAlertIncluir();
	 nprocesso = pedido.numeroProcesso();
	 System.out.println(nprocesso);
	 pedido.clickBtnFecharImprimir();
	 loginPage.navegarPara();
	 loginPage.loginAs("master", "teste");
	 consultapedido.navegarParaOutraPagina(consultapedido);
	 consultapedido.comboOpcaoDePesquisa(ConsultaPedidosPage.assuntoICMSNotaFiscalCorrecao);
	 consultapedido.digitarNumeroProcesso(nprocesso);
	 consultapedido.clickBtnPesquisar();
	 // Atribuindo o processo a outro fiscal
	 consultapedido.clickMarcarProcessoParaAtribuirFiscal().comboSelecionaFiscal().verificarAlertConfirmacaoFiscal()
	 .verificarAlertAtualizar();
	 // Deferindo
	 consultapedido.clickLupaProcesso().clickMarcarProcesso().clickBtnDeferirNota().digitarMotivoDeferirNota()
	 .clickBtnConfirmarMotivoDeferirNota();
	 consultapedido.verificarAlertAtualizar().clickMarcarProcesso().clickDesfazer().verificarAlertAtualizar()
	 .clickMarcarProcesso();
	 // Deferindo parcialmente
	 consultapedido.clickBtnDeferirParcialmenteNota().digitarMotivoDeferirParcialmenteNota()
	 .clickBtnConfirmarMotivoDeferirParcialmenteNota().verificarAlertAtualizar().clickDesfazer()
	 .verificarAlertAtualizar();
	 // Indeferindo
	 consultapedido.clickMarcarProcesso().clickBtnIndeferirNota().digitarMotivoIndeferirNota()
	 .clickBtnConfirmarMotivoIndeferirNota().verificarAlertAtualizar().clickDesfazer()
	 .verificarAlertAtualizar()
	 // Gravando Pendencias
	 .clickMarcarProcesso().clickBtnGravarPendencias().digitarGravarPendencias()
	 .clickBtnConfirmarGravarPendencias().verificarAlertIncluir();
	 // Enviando Pendências
	 consultapedido.clickBtnEnviarPendencias().clickBtnConfirmarEnviarPendencias().verificarAlertAtualizar();
	
	 //Responder pendência
	 loginPage.navegarPara();
	 loginPage.loginAs("master", "teste");
	 pedido.navegarParaOutraPagina(pedido);
	 pedido.verificaMsgrAlertPedidoComPendencia().clickMenuMinhasPendencias().clickLupaProcesso()
	 .verificaMsgrAlerPendenciaPrazo10Dias().clickBtnIncluirAnexo().anexando().digitarDescricaoPendencia()
	 .clickBtnFinalizarAnexo().verificarAlertIncluir().digitarRespostaDaPendencia()
	 .clickBtnResponderPendencia().verificarAlertAtualizar();
	
	 //Finalizando
	 loginPage.navegarPara();
	 loginPage.loginAs("master", "teste");
	 pedido.navegarParaOutraPagina(consultaPedidoAlteracao);
	 consultaPedidoAlteracao.digitaNumeroProcesso(nprocesso);
	 consultaPedidoAlteracao.clickBotaoPesquisar().verificarAlertConsulta().clickAbaPedidoPendenciaAguardandoAnalise();
	 consultaPedidoAlteracao.clickLupa();
	 consultapedido.clickMarcarProcesso().clickBtnDeferirNota();
	 consultapedido.digitarMotivoDeferirNota().clickBtnConfirmarMotivoDeferirNota().verificarAlertAtualizar()
	 .clickMarcarProcesso();
	 consultapedido.clickBtnFinalizarPedido().clickBtnConfirmar().verificarAlertAtualizar();
	
	 }

	@Test(priority = 3)
	public void fazer_Cancelamento() throws InterruptedException {

		 loginPage.navegarPara();
		 loginPage.loginAs("master", "teste");
		 pedido.navegarParaOutraPagina(pedido);
		 pedido.verificarAlerSecaoExpirada();
		 pedido.comboTipoProcesso("ICMS / NOTA FISCAL - CORRECAO");
		 pedido.clickBtnIncluirNotaFiscal();
		 pedido.comboOpcaoDePesquisa("Nº do selo");
		 pedido.digitarNumeroSelo("201600069076");
		 pedido.clickBtnPesquisar().clickMaisIncluirNota().comboMotivo();
		 pedido.digitarObservacoes().clickBtnIncluir().verificarAlertNotaIncluida().clickBtnFecharPesquisaNota();
		 pedido.clickBtnIncluirAnexo().anexando().digitarDescricao().clickBtnFinalizarAnexo()
		 .verificarAlertIncluirAnexo();
		 pedido.clickBtnEnviarPedido().verificarAlertIncluir();
		 nprocesso = pedido.numeroProcesso();
		 System.out.println(nprocesso);
		 pedido.clickBtnFecharImprimir();
		 loginPage.navegarPara();
		 loginPage.loginAs("master", "teste");
		 consultapedido.navegarParaOutraPagina(consultapedido);
		 pedido.verificarAlerSecaoExpirada();
		 consultapedido.comboOpcaoDePesquisa(ConsultaPedidosPage.assuntoICMSNotaFiscalCorrecao);
		 consultapedido.digitarNumeroProcesso(nprocesso);
		 consultapedido.clickBtnPesquisar();
		// // Atribuindo o processo a outro fiscal
		// consultapedido.clickMarcarProcessoParaAtribuirFiscal().comboSelecionaFiscal().verificarAlertConfirmacaoFiscal()
		// .verificarAlertAtualizar();
		
		 consultapedido.clickLupaProcesso();
		 
		
		 // Gravando Pendencias
		 consultapedido .clickMarcarProcesso().clickBtnGravarPendencias().digitarGravarPendencias();
		 consultapedido.clickBtnConfirmarGravarPendencias().verificarAlertIncluir();
		 // Enviando Pendências
		 consultapedido.clickBtnEnviarPendencias().clickBtnConfirmarEnviarPendencias().verificarAlertAtualizar();

		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		pedido.navegarParaOutraPagina(pedido);
		 pedido.verificarAlerSecaoExpirada();
		pedido.verificaMsgrAlertPedidoComPendencia().clickMenuMinhasPendencias().clickLupaProcesso()
				.verificaMsgrAlerPendenciaPrazo10Dias().clickAbaCancelamentoProcesso().digitarMotivoCancelamento()
				.clickBtnSolicitarCancelamento().verificarAlertSolicitarCancelamento();
		// Cancelando
		 loginPage.navegarPara();
		 loginPage.loginAs("master", "teste");
		 pedido.navegarParaOutraPagina(consultaPedidoAlteracao);
		 pedido.verificarAlerSecaoExpirada();
		 consultaPedidoAlteracao.digitaNumeroProcesso(nprocesso);
		 consultaPedidoAlteracao.clickBotaoPesquisar().verificarAlertConsulta().clickAbaPedidoPendenciaAguardandoCancelamento();
		 consultaPedidoAlteracao.clickMarcarProcessoPlatilheira().clickBtnAceitarCancelamento();
		 consultapedido.verificarAlertAtualizar();
		
		
	}

}
