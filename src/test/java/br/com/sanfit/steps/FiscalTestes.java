package br.com.sanfit.steps;

import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.sanfit.pageobjects.FiscalPage;
import br.com.sanfit.pageobjects.LoginPage;

public class FiscalTestes extends BaseTeste {
	FiscalPage fiscal = new FiscalPage();
	LoginPage loginPage = new LoginPage();

	@Test(priority = 1)
	public void fazer_CadastrarFiscalsDeAlteracoesDeNotasFiscais() throws InterruptedException {
		loginPage.navegarPara();
		loginPage.loginAs("master", "teste");
		fiscal.navegarParaOutraPagina(fiscal);
		fiscal.clickBtnNovoFiscal().clickBtnLupaFiscal().preencherCampoFiscal().clickBtnPesquisar().clickDuplo()
				.clickBtnIncluirFiscal().verificarAlertIncluir();
	}

	@Test(priority = 2)
	public void fazer_CadastrarFiscal() throws InterruptedException {
		fiscal.clickBtnLocalizarFiscal();
		fiscal.digitarFiscal();
		fiscal.clickBtnPesquisarFiscal();
		fiscal.listaDeRegistrosEncontrados();
		fiscal.clicKFecharLocalizarFiscals();

	}

	@Test(priority = 3)
	public void fazer_AtualizarFiscal() throws InterruptedException {
		fiscal.clickGrid();
		fiscal.clickBtnAtualizarFiscal();
		fiscal.verificarAlertAtualizar();
	}

	@Test(priority = 7)
	public void fazer_ExcluirFiscal() throws InterruptedException {
		fiscal.clickGridFiscal();
		fiscal.clickBtnExcluir();
		fiscal.verificarAlertConfirmarExcluir();
		fiscal.verificarAlertExcluir();
	}

	@Test(priority = 8)
	public void fazer_LimparFiscal() throws InterruptedException {
		fiscal.clickGridFiscal();
		fiscal.limparFiscal();
		Assert.assertTrue(fiscal.campoLimpo);
	}

	//
	@Test(priority = 4)
	public void fazer_ExibirInformacoesCadastrais() throws InterruptedException {

		fiscal.clickGridFiscal();
		fiscal.clickBtnInfCadastrais();
		fiscal.informacoesCadastraisFiscal();
		fiscal.clickBtnFecharInformacoesFiscais();

	}

	@Test(priority = 6)
	public void fazer_VisualizarRelatorio() throws InterruptedException {
		fiscal.clickGridFiscal();
		fiscal.clickBtnRelatorio();
		fiscal.clickBtnVisualizarRelatorio();
		fiscal.clickBtnFecharRelatorio();
	}

	@Test(priority = 5)
	public void fazer_FiscalDesativarAtivar() throws InterruptedException {

		fiscal.clickGridFiscal();
		fiscal.clickDesativar();
		fiscal.verificarAlertAtualizar();
		fiscal.listaDeRegistrosEncontrados();
		fiscal.clickGridFiscal();
		fiscal.clickAtivar();
		fiscal.verificarAlertAtualizar();
	}
}
