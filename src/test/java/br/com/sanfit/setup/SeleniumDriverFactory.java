package br.com.sanfit.setup;

import static br.com.sanfit.setup.ConfigUtils.getProperty;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SeleniumDriverFactory {
	private static WebDriver driver;
	private static final String FIREFOX = "firefox";
	private static final String CHROME = "chrome";
	private static String SERVER_URL = "server.url";

	public static WebDriver getDriver() {
		if (driver == null || ((RemoteWebDriver) driver).getSessionId() == null) {
			createDriver();
		}
		return driver;
	}

	
		@Before
	private static WebDriver createDriver() {
		String property = getProperty("driver", FIREFOX);

		if (property.equals(FIREFOX)) {
			File file = new File("src/test/resources/config/geckodriver.exe");
			System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			
		} else if (property.equals(CHROME)) {
			File file = new File("src/test/resources/config/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getPath());
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			
		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		return driver;
	}
		
		@After
		public static void sair() {
			driver.quit();
		}

	public static String getServerUrl() {
		return getProperty(SERVER_URL, "");
	}

}
