package br.com.sanfit.setup;

import static java.lang.System.getenv;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.FileReader;
import java.util.Properties;

public class ConfigUtils {
    private static Properties config;
    private static String SERVER_URL = "server.url";
    private static final String CONFIG_FILE = "src/test/resources/config/config.properties";

    private static Properties load() throws Exception {
        config = new Properties();
        if (config.isEmpty()) {
            config.load(new FileReader(CONFIG_FILE));
        }
        return config;
    }

    public static String getProperty(String property, String defaultValue) {
        String result = null;

        try {
            result = getenv(property);
            if (isBlank(result)) {
                result = System.getProperty(property);
            }
            if (isBlank(result)) {
                Properties file = load();
                result = file.getProperty(property, defaultValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public static String getServerUrl() {
        return getProperty(SERVER_URL, "");
    }
}
